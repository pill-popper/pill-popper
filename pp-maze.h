/* pp-maze.h */

#ifndef _PP_MAZE_H
#define _PP_MAZE_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "pp-guy.h"
#include "pp-types.h"

G_BEGIN_DECLS

#define PP_TYPE_MAZE pp_maze_get_type()

#define PP_MAZE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  PP_TYPE_MAZE, PPMaze))

#define PP_MAZE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  PP_TYPE_MAZE, PPMazeClass))

#define PP_IS_MAZE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  PP_TYPE_MAZE))

#define PP_IS_MAZE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  PP_TYPE_MAZE))

#define PP_MAZE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  PP_TYPE_MAZE, PPMazeClass))

typedef struct _PPMazeClass PPMazeClass;
typedef struct _PPMazePrivate PPMazePrivate;

struct _PPMaze
{
  ClutterActor parent;

  PPMazePrivate *priv;
};

struct _PPMazeClass
{
  ClutterActorClass parent_class;
};

GType pp_maze_get_type (void) G_GNUC_CONST;

ClutterActor *pp_maze_new (void);

void     pp_maze_set_size               (PPMaze *maze, guint width,
                                         guint height);

guint    pp_maze_get_maze_width         (PPMaze *maze);
void     pp_maze_set_maze_width         (PPMaze *maze, guint width);

guint    pp_maze_get_maze_height        (PPMaze *maze);
void     pp_maze_set_maze_height        (PPMaze *maze, guint height);

void     pp_maze_get_color              (PPMaze *maze, ClutterColor *color);
void     pp_maze_set_color              (PPMaze *maze,
                                         const ClutterColor *color);

guint    pp_maze_get_safe               (PPMaze *maze);
void     pp_maze_set_safe               (PPMaze *maze, guint msecs);

void     pp_maze_set_maze               (PPMaze *maze, const guchar *data);
guchar * pp_maze_peek_maze              (PPMaze *maze);
void     pp_maze_reset                  (PPMaze *maze);
gboolean pp_maze_find_tile              (PPMaze *maze, PPMazeTileCode code,
                                         gint *x, gint *y);

void     pp_maze_pause                  (PPMaze *maze);
void     pp_maze_play                   (PPMaze *maze);

void     pp_maze_add_guy                (PPMaze *maze, PPGuy *guy,
                                         PPMazeTileCode spawn_point);
void     pp_maze_remove_guy             (PPMaze *maze, PPGuy *guy);
GList  * pp_maze_get_guys               (PPMaze *maze);

PPDirection    pp_maze_guy_get_direction      (PPMaze *maze, PPGuy *guy);
PPMazeTileCode pp_maze_guy_get_tile           (PPMaze *maze, PPGuy *guy);
gboolean       pp_maze_guy_validate_direction (PPMaze *maze, PPGuy *guy,
                                               PPDirection direction);
void     pp_maze_guy_get_position       (PPMaze *maze, PPGuy *guy,
                                         gint *x, gint *y);
PPMazeTileCode pp_maze_guy_get_spawn_point (PPMaze *maze, PPGuy *guy);

G_END_DECLS

#endif /* _PP_MAZE_H */
