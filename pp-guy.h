/* pp-guy.h */

#ifndef _PP_GUY_H
#define _PP_GUY_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "pp-types.h"

G_BEGIN_DECLS

#define PP_GUY_DEFAULT_SPEED 200

#define PP_TYPE_GUY pp_guy_get_type()

#define PP_GUY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  PP_TYPE_GUY, PPGuy))

#define PP_GUY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  PP_TYPE_GUY, PPGuyClass))

#define PP_IS_GUY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  PP_TYPE_GUY))

#define PP_IS_GUY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  PP_TYPE_GUY))

#define PP_GUY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  PP_TYPE_GUY, PPGuyClass))

typedef struct _PPGuyClass PPGuyClass;
typedef struct _PPGuyPrivate PPGuyPrivate;

struct _PPGuy
{
  ClutterActor parent;

  PPGuyPrivate *priv;
};

struct _PPGuyClass
{
  ClutterActorClass parent_class;

  /* vfuncs */
  gboolean (* is_dead)      (PPGuy *guy);
  gboolean (* pass_through) (PPGuy *guy, PPMazeTileCode code);
  void     (* decide)       (PPGuy *guy);

  /* signals */
  void     (* collide)      (PPGuy *guy, PPGuy *collidee);
  void     (* die)          (PPGuy *guy);
  void     (* spawn)        (PPGuy *guy);
  void     (* passing)      (PPGuy *guy, PPMazeTileCode *code);
  void     (* maze_set)     (PPGuy *guy, PPMaze *maze, PPMaze *old_maze);
};

GType pp_guy_get_type (void) G_GNUC_CONST;

ClutterActor *pp_guy_new (void);

PPDirection pp_guy_get_direction (PPGuy *guy);
void        pp_guy_set_direction (PPGuy *guy, PPDirection direction);

gdouble     pp_guy_get_movement  (PPGuy *guy);
void        pp_guy_set_movement  (PPGuy *guy, gdouble movement);

guint       pp_guy_get_speed     (PPGuy *guy);
void        pp_guy_set_speed     (PPGuy *guy, guint speed);

PPMaze     *pp_guy_get_maze      (PPGuy *guy);

void        pp_guy_collide       (PPGuy *guy, PPGuy *collidee);
void        pp_guy_die           (PPGuy *guy);
gboolean    pp_guy_is_dead       (PPGuy *guy);
void        pp_guy_spawn         (PPGuy *guy);
gboolean    pp_guy_pass_through  (PPGuy *guy, PPMazeTileCode code);
void        pp_guy_decide        (PPGuy *guy);
void        pp_guy_passing       (PPGuy *guy, PPMazeTileCode *code);

G_END_DECLS

#endif /* _PP_GUY_H */

