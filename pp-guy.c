/* pp-guy.c */

#include "pp-guy.h"
#include "pp-maze.h"
#include "pp-marshal.h"

G_DEFINE_TYPE (PPGuy, pp_guy, CLUTTER_TYPE_ACTOR)

#define GUY_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), PP_TYPE_GUY, PPGuyPrivate))

struct _PPGuyPrivate
{
  PPDirection  direction;
  gdouble      movement;
  guint        speed;
  PPMaze      *maze;
};

enum
{
  PROP_0,

  PROP_DIRECTION,
  PROP_MOVEMENT,
  PROP_SPEED
};

enum
{
  COLLIDE,
  DIE,
  SPAWN,
  PASSING,
  MAZE_SET,

  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };

const GEnumValue PPEnumDirection[] =
{
  { PP_STATIONARY, "stationary", "Stationary" },
  { PP_UP, "up", "Up" },
  { PP_DOWN, "down", "Down" },
  { PP_LEFT, "left", "Left" },
  { PP_RIGHT, "right", "Right" },
  { 0, NULL, NULL }
};

static void
pp_guy_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  PPGuy *guy = PP_GUY (object);

  switch (property_id)
    {
    case PROP_DIRECTION:
      g_value_set_enum (value, pp_guy_get_direction (guy));
      break;

    case PROP_MOVEMENT:
      g_value_set_double (value, pp_guy_get_movement (guy));
      break;

    case PROP_SPEED:
      g_value_set_uint (value, pp_guy_get_speed (guy));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
pp_guy_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  PPGuy *guy = PP_GUY (object);

  switch (property_id)
    {
    case PROP_DIRECTION:
      pp_guy_set_direction (guy, g_value_get_enum (value));
      break;

    case PROP_MOVEMENT:
      pp_guy_set_movement (guy, g_value_get_double (value));
      break;

    case PROP_SPEED:
      pp_guy_set_speed (guy, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
pp_guy_dispose (GObject *object)
{
  G_OBJECT_CLASS (pp_guy_parent_class)->dispose (object);
}

static void
pp_guy_finalize (GObject *object)
{
  G_OBJECT_CLASS (pp_guy_parent_class)->finalize (object);
}

static void
pp_guy_real_collide (PPGuy *self, PPGuy *collidee)
{
}

static void
pp_guy_real_die (PPGuy *self)
{
}

static gboolean
pp_guy_real_is_dead (PPGuy *self)
{
  return FALSE;
}

static void
pp_guy_real_spawn (PPGuy *self)
{
}

static gboolean
pp_guy_real_pass_through (PPGuy *self, PPMazeTileCode code)
{
  if (code == PP_WALL)
    return FALSE;
  else
    return TRUE;
}

static void
pp_guy_real_decide (PPGuy *self)
{
}

static void
pp_guy_real_passing (PPGuy *self, PPMazeTileCode *code)
{
}

static void
pp_guy_real_maze_set (PPGuy *self, PPMaze *maze, PPMaze *old_maze)
{
}

static void
pp_guy_class_init (PPGuyClass *klass)
{
  GType direction_type;
  GParamSpec *pspec;

  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (PPGuyPrivate));

  object_class->get_property = pp_guy_get_property;
  object_class->set_property = pp_guy_set_property;
  object_class->dispose = pp_guy_dispose;
  object_class->finalize = pp_guy_finalize;

  klass->collide      = pp_guy_real_collide;
  klass->die          = pp_guy_real_die;
  klass->is_dead      = pp_guy_real_is_dead;
  klass->spawn        = pp_guy_real_spawn;
  klass->pass_through = pp_guy_real_pass_through;
  klass->decide       = pp_guy_real_decide;
  klass->passing      = pp_guy_real_passing;
  klass->maze_set     = pp_guy_real_maze_set;

  direction_type = g_enum_register_static ("PPDirection",
                                           PPEnumDirection);

  pspec = g_param_spec_enum ("direction",
                             "Direction",
                             "The direction of the guy.",
                             direction_type, PP_STATIONARY,
                             G_PARAM_READWRITE |
                             G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_DIRECTION, pspec);

  pspec = g_param_spec_double ("movement",
                               "Movement",
                               "The movement progress of the guy.",
                               0.0, 1.0, 0.0,
                               G_PARAM_READWRITE |
                               G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_MOVEMENT, pspec);

  pspec = g_param_spec_uint ("speed",
                             "Speed",
                             "Amount of time taken to go between tiles.",
                             100, G_MAXUINT, PP_GUY_DEFAULT_SPEED,
                             G_PARAM_READWRITE |
                             G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_SPEED, pspec);

  signals[COLLIDE] =
    g_signal_new ("collide",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PPGuyClass, collide),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1,
                  G_TYPE_POINTER);

  signals[DIE] =
    g_signal_new ("die",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PPGuyClass, die),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

  signals[SPAWN] =
    g_signal_new ("spawn",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PPGuyClass, spawn),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

  signals[PASSING] =
    g_signal_new ("passing",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PPGuyClass, passing),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1,
                  G_TYPE_POINTER);

  signals[MAZE_SET] =
    g_signal_new ("maze-set",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PPGuyClass, maze_set),
                  NULL, NULL,
                  pp_marshal_VOID__POINTER_POINTER,
                  G_TYPE_NONE, 2,
                  G_TYPE_POINTER, G_TYPE_POINTER);
}

static void
pp_guy_parent_set_cb (ClutterActor *self)
{
  PPMaze *new_maze;

  PPGuyPrivate *priv = PP_GUY (self)->priv;
  ClutterActor *parent = clutter_actor_get_parent (self);

  if (parent && PP_IS_MAZE (parent))
    new_maze = (PPMaze *)parent;
  else
    new_maze = NULL;

  g_signal_emit (self, signals[MAZE_SET], 0, new_maze, priv->maze);

  priv->maze = new_maze;
}

static void
pp_guy_init (PPGuy *self)
{
  PPGuyPrivate *priv = self->priv = GUY_PRIVATE (self);

  priv->speed = PP_GUY_DEFAULT_SPEED;

  g_signal_connect (self, "parent-set",
                    G_CALLBACK (pp_guy_parent_set_cb), NULL);
}

ClutterActor *
pp_guy_new (void)
{
  return g_object_new (PP_TYPE_GUY, NULL);
}

PPDirection
pp_guy_get_direction (PPGuy *guy)
{
  return guy->priv->direction;
}

void
pp_guy_set_direction (PPGuy *guy, PPDirection direction)
{
  PPGuyPrivate *priv = guy->priv;

  if (priv->direction != direction)
    {
      priv->direction = direction;
      g_object_notify (G_OBJECT (guy), "direction");
      clutter_actor_queue_relayout (CLUTTER_ACTOR (guy));
    }
}

gdouble
pp_guy_get_movement (PPGuy *guy)
{
  return guy->priv->movement;
}

void
pp_guy_set_movement (PPGuy *guy, gdouble movement)
{
  PPGuyPrivate *priv = guy->priv;

  if (priv->movement != movement)
    {
      priv->movement = movement;
      g_object_notify (G_OBJECT (guy), "movement");
      clutter_actor_queue_redraw (CLUTTER_ACTOR (guy));
    }
}

guint
pp_guy_get_speed (PPGuy *guy)
{
  return guy->priv->speed;
}

void
pp_guy_set_speed (PPGuy *guy, guint speed)
{
  PPGuyPrivate *priv = guy->priv;

  if (priv->speed != speed)
    {
      priv->speed = speed;
      g_object_notify (G_OBJECT (guy), "speed");
    }
}

PPMaze *
pp_guy_get_maze (PPGuy *guy)
{
  return guy->priv->maze;
}

void
pp_guy_collide (PPGuy *guy, PPGuy *collidee)
{
  g_signal_emit (guy, signals[COLLIDE], 0, collidee);
}

void
pp_guy_die (PPGuy *guy)
{
  g_signal_emit (guy, signals[DIE], 0);
}

gboolean
pp_guy_is_dead (PPGuy *guy)
{
  return PP_GUY_GET_CLASS (guy)->is_dead (guy);
}

void
pp_guy_spawn (PPGuy *guy)
{
  g_signal_emit (guy, signals[SPAWN], 0);
}

gboolean
pp_guy_pass_through (PPGuy *guy, PPMazeTileCode code)
{
  return PP_GUY_GET_CLASS (guy)->pass_through (guy, code);
}

void
pp_guy_decide (PPGuy *guy)
{
  PP_GUY_GET_CLASS (guy)->decide (guy);
}

void
pp_guy_passing (PPGuy *guy, PPMazeTileCode *code)
{
  g_signal_emit (guy, signals[PASSING], 0, code);
  PP_GUY_GET_CLASS (guy)->passing (guy, code);
}
