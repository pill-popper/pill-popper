/* pp-enemy.c */

#include "pp-enemy.h"
#include "pp-maze.h"
#include "pp-utils.h"
#include "pp-marshal.h"

G_DEFINE_TYPE (PPEnemy, pp_enemy, PP_TYPE_GUY)

#define ENEMY_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), PP_TYPE_ENEMY, PPEnemyPrivate))

struct _PPEnemyPrivate
{
  ClutterColor  color;
  gboolean      passed_barrier;
  gboolean      dead;
  gboolean      vulnerable;
};

enum
{
  PROP_0,

  PROP_COLOR
};

enum
{
  TARGETTING,

  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };

static const ClutterColor red   = { 0xff, 0x00, 0x00, 0xff };
static const ClutterColor white = { 0xff, 0xff, 0xff, 0xff };
static const ClutterColor blue  = { 0x21, 0x21, 0xde, 0xff };
static const ClutterColor black = { 0x00, 0x00, 0x00, 0xff };

static void
pp_enemy_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  PPEnemyPrivate *priv = PP_ENEMY (object)->priv;

  switch (property_id)
    {
    case PROP_COLOR:
      clutter_value_set_color (value, &priv->color);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
pp_enemy_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  PPEnemyPrivate *priv = PP_ENEMY (object)->priv;

  switch (property_id)
    {
    case PROP_COLOR:
      priv->color = *(clutter_value_get_color (value));
      clutter_actor_queue_redraw (CLUTTER_ACTOR (object));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
pp_enemy_dispose (GObject *object)
{
  G_OBJECT_CLASS (pp_enemy_parent_class)->dispose (object);
}

static void
pp_enemy_finalize (GObject *object)
{
  G_OBJECT_CLASS (pp_enemy_parent_class)->finalize (object);
}

static void
pp_enemy_paint (ClutterActor *actor)
{
  gint i;
  guint safe;
  PPMaze *maze;
  gfloat offset_x, offset_y;
  const ClutterColor *color;
  const ClutterColor *secondary;
  PPEnemyPrivate *priv = PP_ENEMY (actor)->priv;

  maze = pp_guy_get_maze (PP_GUY (actor));
  safe = pp_maze_get_safe (maze);

  secondary = &white;
  if (!priv->dead)
    {
      if (priv->vulnerable)
        {
          if (safe < 3000)
            {
              if ((safe / 500) % 2)
                {
                  color = &white;
                  secondary = &black;
                }
              else
                color = &blue;
            }
          else
            color = &blue;
        }
      else
        color = &priv->color;

      cogl_set_source_color4ub (color->red,
                                color->green,
                                color->blue,
                                color->alpha);

      cogl_path_new ();

      /* Draw head arc */
      cogl_path_move_to (0.0, 0.5);
      cogl_path_arc (0.5, 0.5, 0.5, 0.5,
                     180, 360);

      /* Side line */
      cogl_path_line_to (1.0, 1.0);

      /* Zig-zag */
      for (i = 3; i >= 0; i--)
        {
          cogl_path_line_to (i/4.f + 1/8.f, 0.9);
          cogl_path_line_to (i/4.f, 1.0);
        }

      /* Other side */
      cogl_path_close ();

      /* Fill */
      cogl_path_fill ();
    }

  /* Eyeball */
  cogl_set_source_color4ub (secondary->red,
                            secondary->green,
                            secondary->blue,
                            secondary->alpha);
  /* Left */
  cogl_path_arc (1/4.f, 0.4, 1/6.f, 1/6.f, 0, 360);
  cogl_path_fill ();
  /* Right */
  cogl_path_arc (1.f - 1/4.f, 0.4, 1/6.f, 1/6.f, 0, 360);
  cogl_path_fill ();

  if (priv->vulnerable && !priv->dead)
    {
      /* Zig-zag mouth */
      cogl_path_move_to (0, 0.70);
      for (i = 1; i <= 3; i++)
        {
          cogl_path_line_to (i/3.f - 1/6.f, 0.80);
          cogl_path_line_to (i/3.f, 0.70);
        }
      cogl_path_stroke ();
    }
  else
    {
      /* Pupil */
      offset_x = offset_y = 0.0;
      switch (pp_guy_get_direction (PP_GUY (actor)))
        {
        case PP_UP:
          offset_y -= 1/8.f;
          break;
        case PP_RIGHT:
          offset_x += 1/8.f;
          break;
        case PP_DOWN:
          offset_y += 1/8.f;
          break;
        case PP_LEFT:
          offset_x -= 1/8.f;
          break;
        default:
          break;
        }

      cogl_set_source_color4ub (blue.red,
                                blue.green,
                                blue.blue,
                                blue.alpha);

      cogl_path_arc (1/4.f + offset_x, 0.4 + offset_y, 1/9.f, 1/9.f, 0, 360);
      cogl_path_fill ();

      cogl_path_arc (1.f - 1/4.f + offset_x, 0.4 + offset_y, 1/9.f, 1/9.f, 0, 360);
      cogl_path_fill ();
    }
}

static void
pp_enemy_collide (PPGuy *guy, PPGuy *collidee)
{
  PPEnemyPrivate *priv = PP_ENEMY (guy)->priv;

  if (!PP_IS_ENEMY (collidee) &&
      priv->vulnerable &&
      !pp_guy_is_dead (collidee) &&
      !pp_guy_is_dead (guy))
    pp_guy_die (guy);
}

static gboolean
pp_enemy_pass_through (PPGuy *guy, PPMazeTileCode code)
{
  PPEnemyPrivate *priv = PP_ENEMY (guy)->priv;

  if (code == PP_BARRIER_PLAYER)
    {
      if (!priv->passed_barrier)
        return TRUE;
      else
        return FALSE;
    }
  else if (code == PP_BARRIER_ENEMY)
    return FALSE;
  else
    return PP_GUY_CLASS (pp_enemy_parent_class)->pass_through (guy, code);
}

static void
pp_enemy_decide (PPGuy *guy)
{
  PPEnemyPrivate *priv = PP_ENEMY (guy)->priv;
  PPMaze *maze = pp_guy_get_maze (guy);

  /* Continue through any barriers */
  if (pp_maze_guy_get_tile (maze, guy) == PP_BARRIER_PLAYER)
    {
      if (!priv->dead)
        priv->passed_barrier = TRUE;
      else
        {
          pp_guy_set_speed (guy, PP_GUY_DEFAULT_SPEED);
          priv->dead = FALSE;
        }

      return;
    }

  if (!priv->dead && !priv->passed_barrier)
    {
      /* We're inside the spawn area */

      /* Pick a random direction if we've ended a move */
      PPDirection direction;
      do {
        direction = pp_random_direction ();
      } while (!pp_maze_guy_validate_direction (maze, guy, direction));

      pp_guy_set_direction (guy, direction);
    }
  else
    {
      /* We're in the maze */
      gint i, x, y, target_x, target_y, score;
      PPDirection new_direction;
      PPDirection direction = pp_maze_guy_get_direction (maze, guy);

      pp_maze_guy_get_position (maze, guy, &x, &y);

      /* Get our target */
      /* If we're dead, head for the spawn point */
      target_x = target_y = 0;
      if (!priv->dead)
        g_signal_emit (guy, signals[TARGETTING], 0, &target_x, &target_y);
      else
        pp_maze_find_tile (maze, PP_BARRIER_PLAYER, &target_x, &target_y);

      /* Figure out a way to get there (not necessarily the quickest) */
      score = G_MAXINT;
      new_direction = direction;
      for (i = 0; i < 4; i++)
        {
          gint new_score;
          PPDirection test_direction;

          switch (i)
            {
            case 0:
              test_direction = PP_UP;
              new_score = ABS (x - target_x) + ABS ((y - 1) - target_y);
              break;
            case 1:
              test_direction = PP_LEFT;
              new_score = ABS ((x - 1) - target_x) + ABS (y - target_y);
              break;
            case 2:
              test_direction = PP_DOWN;
              new_score = ABS (x - target_x) + ABS ((y + 1) - target_y);
              break;
            case 3:
              test_direction = PP_RIGHT;
              new_score = ABS ((x + 1) - target_x) + ABS (y - target_y);
              break;
            }

          /* Don't allow turning around */
          if (pp_direction_is_opposite (direction, test_direction))
            continue;

          /* Choose this direction if it's more highly rated */
          if (pp_maze_guy_validate_direction (maze, guy, test_direction) &&
              (new_score < score))
            {
              new_direction = test_direction;
              score = new_score;
            }
        }

      pp_guy_set_direction (guy, new_direction);
    }
}

static void
pp_enemy_die (PPGuy *guy)
{
  PPEnemyPrivate *priv = PP_ENEMY (guy)->priv;

  priv->passed_barrier = FALSE;
  priv->vulnerable = FALSE;
  priv->dead = TRUE;
  pp_guy_set_speed (guy, PP_GUY_DEFAULT_SPEED / 2);
}

static gboolean
pp_enemy_is_dead (PPGuy *guy)
{
  PPEnemyPrivate *priv = PP_ENEMY (guy)->priv;
  return priv->dead;
}

static void
pp_enemy_spawn (PPGuy *guy)
{
  PPEnemyPrivate *priv = PP_ENEMY (guy)->priv;

  priv->passed_barrier = FALSE;
  priv->dead = FALSE;

  /* TODO: Something better here? */
  if (g_random_int_range (0, 2))
    pp_guy_set_direction (guy, PP_LEFT);
  else
    pp_guy_set_direction (guy, PP_RIGHT);
}

static void
pp_enemy_maze_safe_notify (PPMaze     *maze,
                           GParamSpec *pspec,
                           PPEnemy    *self)
{
  gboolean safe;
  PPEnemyPrivate *priv = self->priv;

  safe = pp_maze_get_safe (maze) ? TRUE : FALSE;
  if ((priv->vulnerable != safe) &&
      !priv->dead &&
      priv->passed_barrier)
    {
      PPGuy *guy = PP_GUY (self);

      priv->vulnerable = safe;
      pp_guy_set_speed (guy,
                        safe ? PP_GUY_DEFAULT_SPEED * 2 :
                               PP_GUY_DEFAULT_SPEED);
      clutter_actor_queue_redraw (CLUTTER_ACTOR (self));
    }
}

static void
pp_enemy_maze_set (PPGuy *self, PPMaze *maze, PPMaze *old_maze)
{
  if (old_maze)
    g_signal_handlers_disconnect_by_func (old_maze,
                                          pp_enemy_maze_safe_notify,
                                          self);
  if (maze)
    g_signal_connect (maze, "notify::safe",
                      G_CALLBACK (pp_enemy_maze_safe_notify), self);
}

static void
pp_enemy_class_init (PPEnemyClass *klass)
{
  GParamSpec *pspec;

  PPGuyClass *guy_class = PP_GUY_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  g_type_class_add_private (klass, sizeof (PPEnemyPrivate));

  object_class->get_property = pp_enemy_get_property;
  object_class->set_property = pp_enemy_set_property;
  object_class->dispose = pp_enemy_dispose;
  object_class->finalize = pp_enemy_finalize;

  actor_class->paint = pp_enemy_paint;

  guy_class->collide = pp_enemy_collide;
  guy_class->pass_through = pp_enemy_pass_through;
  guy_class->die = pp_enemy_die;
  guy_class->is_dead = pp_enemy_is_dead;
  guy_class->spawn = pp_enemy_spawn;
  guy_class->decide = pp_enemy_decide;
  guy_class->maze_set = pp_enemy_maze_set;

  pspec = clutter_param_spec_color ("color",
                                    "Enemy color",
                                    "Color of the enemy",
                                    &red,
                                    G_PARAM_READWRITE |
                                    G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_COLOR, pspec);

  signals[TARGETTING] =
    g_signal_new ("targetting",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PPEnemyClass, targetting),
                  NULL, NULL,
                  pp_marshal_VOID__POINTER_POINTER,
                  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);
}

static void
pp_enemy_init (PPEnemy *self)
{
  PPEnemyPrivate *priv = self->priv = ENEMY_PRIVATE (self);

  priv->color = red;
}

ClutterActor *
pp_enemy_new ()
{
  return g_object_new (PP_TYPE_ENEMY, NULL);
}

gboolean
pp_enemy_is_vulnerable (PPEnemy *enemy)
{
  return enemy->priv->vulnerable;
}

