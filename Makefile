
default: pp

clean:
	rm -f pp

H_SRCS = \
	 pp-types.h \
	 pp-maze.h \
	 pp-guy.h \
	 pp-enemy.h \
	 pp-player.h \
	 pp-edge-aa.h \
	 pp-super-aa.h \
	 pp-utils.h \
	 pp-marshal.h

C_SRCS = \
	 pp-main.c \
	 pp-maze.c \
	 pp-guy.c \
	 pp-enemy.c \
	 pp-player.c \
	 pp-edge-aa.c \
	 pp-super-aa.c \
	 pp-utils.c \
	 pp-marshal.c

pp-marshal.c: pp-marshal.list
	glib-genmarshal --body --prefix=pp_marshal pp-marshal.list > pp-marshal.c

pp-marshal.h: pp-marshal.list
	glib-genmarshal --header --prefix=pp_marshal pp-marshal.list > pp-marshal.h

pp: $(C_SRCS) $(H_SRCS)
	$(CC) -o pp $(C_SRCS) -Wall -g -O0 `pkg-config --cflags --libs mx-1.0`
