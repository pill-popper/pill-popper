/* pp-player.h */

#ifndef _PP_PLAYER_H
#define _PP_PLAYER_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "pp-guy.h"

G_BEGIN_DECLS

#define PP_TYPE_PLAYER pp_player_get_type()

#define PP_PLAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  PP_TYPE_PLAYER, PPPlayer))

#define PP_PLAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  PP_TYPE_PLAYER, PPPlayerClass))

#define PP_IS_PLAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  PP_TYPE_PLAYER))

#define PP_IS_PLAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  PP_TYPE_PLAYER))

#define PP_PLAYER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  PP_TYPE_PLAYER, PPPlayerClass))

typedef struct _PPPlayer PPPlayer;
typedef struct _PPPlayerClass PPPlayerClass;
typedef struct _PPPlayerPrivate PPPlayerPrivate;

struct _PPPlayer
{
  PPGuy parent;

  PPPlayerPrivate *priv;
};

struct _PPPlayerClass
{
  PPGuyClass parent_class;
};

GType pp_player_get_type (void) G_GNUC_CONST;

ClutterActor *pp_player_new (void);

void        pp_player_set_direction (PPPlayer    *player,
                                     PPDirection  direction,
                                     gboolean     unset);

G_END_DECLS

#endif /* _PP_PLAYER_H */
