
#include "pp-utils.h"

gboolean
pp_direction_is_opposite (PPDirection direction1,
                          PPDirection direction2)
{
  switch (direction1)
    {
    case PP_UP:
      return (direction2 == PP_DOWN);
    case PP_DOWN:
      return (direction2 == PP_UP);
    case PP_LEFT:
      return (direction2 == PP_RIGHT);
    case PP_RIGHT:
      return (direction2 == PP_LEFT);
    default:
      return FALSE;
    }
}

PPDirection
pp_random_direction ()
{
  switch (g_random_int_range (0, 4))
    {
    case 0:
      return PP_UP;
    case 1:
      return PP_RIGHT;
    case 2:
      return PP_DOWN;
    case 3:
      return PP_LEFT;
    default:
      return PP_STATIONARY;
    }
}
