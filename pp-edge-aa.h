/* pp-edge-aa.h */

#ifndef _PP_EDGE_AA_H
#define _PP_EDGE_AA_H

#include <glib-object.h>
#include <mx/mx.h>

G_BEGIN_DECLS

#define PP_TYPE_EDGE_AA pp_edge_aa_get_type()

#define PP_EDGE_AA(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  PP_TYPE_EDGE_AA, PPEdgeAA))

#define PP_EDGE_AA_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  PP_TYPE_EDGE_AA, PPEdgeAAClass))

#define PP_IS_EDGE_AA(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  PP_TYPE_EDGE_AA))

#define PP_IS_EDGE_AA_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  PP_TYPE_EDGE_AA))

#define PP_EDGE_AA_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  PP_TYPE_EDGE_AA, PPEdgeAAClass))

typedef struct _PPEdgeAA PPEdgeAA;
typedef struct _PPEdgeAAClass PPEdgeAAClass;

struct _PPEdgeAA
{
  MxOffscreen parent;
};

struct _PPEdgeAAClass
{
  MxOffscreenClass parent_class;
};

GType pp_edge_aa_get_type (void) G_GNUC_CONST;

ClutterActor *pp_edge_aa_new ();

G_END_DECLS

#endif /* _PP_EDGE_AA_H */
