/* pp-player.c */

#include "pp-player.h"
#include "pp-maze.h"
#include "pp-enemy.h"

G_DEFINE_TYPE (PPPlayer, pp_player, PP_TYPE_GUY)

#define PLAYER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), PP_TYPE_PLAYER, PPPlayerPrivate))

struct _PPPlayerPrivate
{
  gboolean    dead;
  gdouble     mouth;
  PPDirection last_direction;
};

enum
{
  PROP_0,

  PROP_MOUTH
};

static void
pp_player_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  PPPlayerPrivate *priv = PP_PLAYER (object)->priv;

  switch (property_id)
    {
    case PROP_MOUTH:
      g_value_set_double (value, priv->mouth);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
pp_player_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  PPPlayerPrivate *priv = PP_PLAYER (object)->priv;

  switch (property_id)
    {
    case PROP_MOUTH:
      priv->mouth = g_value_get_double (value);
      clutter_actor_queue_redraw (CLUTTER_ACTOR (object));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
pp_player_dispose (GObject *object)
{
  G_OBJECT_CLASS (pp_player_parent_class)->dispose (object);
}

static void
pp_player_finalize (GObject *object)
{
  G_OBJECT_CLASS (pp_player_parent_class)->finalize (object);
}

static void
pp_player_paint (ClutterActor *actor)
{
  PPPlayer *self = PP_PLAYER (actor);
  PPPlayerPrivate *priv = self->priv;

  if (priv->mouth < 1.0)
    {
      PPMaze *maze;
      gfloat start_angle;
      PPDirection direction;
      cogl_set_source_color4ub (0xff, 0xff, 0x00, 0xff);

      /* Get our real direction, or if we're stationary, our last
       * set direction.
       */
      maze = pp_guy_get_maze (PP_GUY (self));
      direction = maze ? pp_maze_guy_get_direction (maze, PP_GUY (self)) :
                         PP_RIGHT;

      if (direction == PP_STATIONARY)
        direction = pp_guy_get_direction (PP_GUY (self));

      switch (direction)
        {
        case PP_DOWN:
          start_angle = 90;
          break;
        case PP_LEFT:
          start_angle = 180;
          break;
        case PP_UP:
          start_angle = 270;
          break;
        default:
          start_angle = 0;
          break;
        }

      cogl_path_new ();
      cogl_path_move_to (0.5, 0.5);
      cogl_path_arc (0.5, 0.5,
                     0.5, 0.5,
                     start_angle + (priv->mouth * 180.0),
                     start_angle + 360.0 - (priv->mouth * 180.0));
      cogl_path_close ();

      cogl_path_fill ();
    }
}

static void
pp_player_animate_mouth_cb (ClutterAnimation *animation,
                            PPPlayer         *self)
{
  PPPlayerPrivate *priv = self->priv;

  if (priv->mouth > 0.0)
    clutter_actor_animate (CLUTTER_ACTOR (self), CLUTTER_LINEAR, 50,
                           "mouth", 0.0,
                           "signal-after::completed",
                             pp_player_animate_mouth_cb, self,
                           NULL);
  else
    clutter_actor_animate (CLUTTER_ACTOR (self), CLUTTER_LINEAR, 50,
                           "mouth", 0.2,
                           "signal-after::completed",
                             pp_player_animate_mouth_cb, self,
                           NULL);
}

static void
pp_player_die (PPGuy *guy)
{
  ClutterAnimation *animation;
  PPPlayerPrivate *priv = PP_PLAYER (guy)->priv;

  animation = clutter_actor_get_animation (CLUTTER_ACTOR (guy));

  if (animation)
    g_signal_handlers_disconnect_by_func (animation,
                                          pp_player_animate_mouth_cb,
                                          guy);

  clutter_actor_animate (CLUTTER_ACTOR (guy), CLUTTER_LINEAR, 500,
                         "mouth", 1.0,
                         NULL);

  priv->dead = TRUE;
}

static gboolean
pp_player_is_dead (PPGuy *guy)
{
  PPPlayerPrivate *priv = PP_PLAYER (guy)->priv;
  return priv->dead;
}

static void
pp_player_spawn (PPGuy *guy)
{
  ClutterAnimation *animation;

  PPPlayer *self = PP_PLAYER (guy);
  PPPlayerPrivate *priv = self->priv;

  /* Stop animation */
  animation = clutter_actor_get_animation (CLUTTER_ACTOR (guy));
  if (animation)
    {
      g_signal_handlers_disconnect_by_func (animation,
                                            pp_player_animate_mouth_cb,
                                            guy);
      clutter_animation_completed (animation);
    }

  priv->dead = FALSE;
  priv->mouth = 0.0;
  clutter_actor_queue_redraw (CLUTTER_ACTOR (guy));
}

static gboolean
pp_player_pass_through (PPGuy *guy, PPMazeTileCode code)
{
  if (code == PP_BARRIER_PLAYER)
    return FALSE;
  else return PP_GUY_CLASS (pp_player_parent_class)->pass_through (guy, code);
}

static void
pp_player_passing (PPGuy *guy, PPMazeTileCode *code)
{
  switch (*code)
    {
    case PP_SUPER_PILL:
      pp_maze_set_safe (pp_guy_get_maze (guy), 10000);
    case PP_PILL:
      *code = PP_EMPTY;
      break;

    default:
      break;
    }
}

static void
pp_player_collide (PPGuy *guy, PPGuy *collidee)
{
  if (PP_IS_ENEMY (collidee))
    {
      if (!pp_enemy_is_vulnerable (PP_ENEMY (collidee)) &&
          !pp_guy_is_dead (collidee) &&
          !pp_guy_is_dead (guy))
        pp_guy_die (guy);
    }
}

static void
pp_player_class_init (PPPlayerClass *klass)
{
  GParamSpec *pspec;

  PPGuyClass *guy_class = PP_GUY_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  g_type_class_add_private (klass, sizeof (PPPlayerPrivate));

  object_class->get_property = pp_player_get_property;
  object_class->set_property = pp_player_set_property;
  object_class->dispose = pp_player_dispose;
  object_class->finalize = pp_player_finalize;

  actor_class->paint = pp_player_paint;

  guy_class->die = pp_player_die;
  guy_class->is_dead = pp_player_is_dead;
  guy_class->spawn = pp_player_spawn;
  guy_class->pass_through = pp_player_pass_through;
  guy_class->passing = pp_player_passing;
  guy_class->collide = pp_player_collide;

  pspec = g_param_spec_double ("mouth",
                               "Mouth",
                               "How much of the player consists of mouth.",
                               0.0, 1.0, 0.0,
                               G_PARAM_READWRITE |
                               G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_MOUTH, pspec);
}

static void
pp_player_direction_notify (PPPlayer *self)
{
  PPMaze *maze;
  PPDirection direction;
  PPPlayerPrivate *priv = self->priv;

  if (!(maze = pp_guy_get_maze (PP_GUY (self))))
    return;

  direction = pp_guy_get_direction (PP_GUY (self));

  if (pp_maze_guy_validate_direction (maze, PP_GUY (self), direction))
    priv->last_direction = direction;
}

static void
pp_player_init (PPPlayer *self)
{
  PPPlayerPrivate *priv = self->priv = PLAYER_PRIVATE (self);

  priv->last_direction = PP_RIGHT;
  g_signal_connect (self, "notify::direction",
                    G_CALLBACK (pp_player_direction_notify), NULL);
}

ClutterActor *
pp_player_new (void)
{
  return g_object_new (PP_TYPE_PLAYER, NULL);
}

void
pp_player_set_direction (PPPlayer    *player,
                         PPDirection  direction,
                         gboolean     unset)
{
  PPGuy *guy = PP_GUY (player);

  /* Start up mouth animation */
  if (!clutter_actor_get_animation (CLUTTER_ACTOR (player)))
    pp_player_animate_mouth_cb (NULL, player);

  /* Unset direction */
  if (direction == PP_STATIONARY || unset)
    {
      PPMaze *maze = pp_guy_get_maze (guy);
      pp_guy_set_direction (guy, pp_maze_guy_get_direction (maze, guy));
      return;
    }

  pp_guy_set_direction (guy, direction);
}
