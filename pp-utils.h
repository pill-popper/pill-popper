
#ifndef _PP_UTILS_H
#define _PP_UTILS_H

#include <glib.h>
#include "pp-types.h"

G_BEGIN_DECLS

gboolean
pp_direction_is_opposite (PPDirection direction1,
                          PPDirection direction2);

PPDirection
pp_random_direction ();

G_END_DECLS

#endif
