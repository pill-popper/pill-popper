
#include <clutter/clutter.h>
#include <mx/mx.h>
#include <GL/gl.h>

#include "pp-maze.h"
#include "pp-player.h"
#include "pp-enemy.h"
#include "pp-edge-aa.h"
#include "pp-super-aa.h"

typedef struct
{
  ClutterActor *screen_box;
  ClutterActor *menu;
  ClutterActor *maze;
  ClutterActor *player;
  ClutterActor *score_label;
  ClutterActor *lives_box;
  gint          score;
} PPData;

static void
pp_main_enemy1_targetting_cb (PPEnemy *enemy,
                              gint    *target_x,
                              gint    *target_y,
                              PPGuy   *player)
{
  PPMaze *maze = pp_guy_get_maze (player);
  pp_maze_guy_get_position (maze, player, target_x, target_y);
}

static void
pp_main_enemy2_targetting_cb (PPEnemy *enemy,
                              gint    *target_x,
                              gint    *target_y,
                              PPGuy   *player)
{
  gint x, y;

  PPMaze *maze = pp_guy_get_maze (player);
  PPDirection direction = pp_maze_guy_get_direction (maze, player);

  pp_maze_guy_get_position (maze, player, &x, &y);

  switch (direction)
    {
    case PP_UP:
      y -= 4;
      break;
    case PP_RIGHT:
      x += 4;
      break;
    case PP_DOWN:
      y += 4;
      break;
    case PP_LEFT:
      x -= 4;
      break;
    default:
      break;
    }

  if (target_x)
    *target_x = x;
  if (target_y)
    *target_y = y;
}

static void
pp_main_enemy3_targetting_cb (PPEnemy   *enemy,
                              gint      *target_x,
                              gint      *target_y,
                              PPGuy    **guys)
{
  gint x, y, x2, y2;

  PPMaze *maze = pp_guy_get_maze (guys[0]);
  PPDirection direction = pp_maze_guy_get_direction (maze, guys[0]);

  pp_maze_guy_get_position (maze, guys[0], &x, &y);
  pp_maze_guy_get_position (maze, guys[1], &x2, &y2);

  switch (direction)
    {
    case PP_UP:
      y -= 2;
      break;
    case PP_RIGHT:
      x += 2;
      break;
    case PP_DOWN:
      y += 2;
      break;
    case PP_LEFT:
      x -= 2;
      break;
    default:
      break;
    }

  x += (x - x2);
  y += (y - y2);

  if (target_x)
    *target_x = x;
  if (target_y)
    *target_y = y;
}

static void
pp_main_enemy4_targetting_cb (PPGuy *enemy,
                              gint  *target_x,
                              gint  *target_y,
                              PPGuy *player)
{
  gint x, y, x2, y2;

  PPMaze *maze = pp_guy_get_maze (player);

  pp_maze_guy_get_position (maze, player, &x, &y);
  pp_maze_guy_get_position (maze, enemy, &x2, &y2);

  if ((ABS (x - x2) + ABS (y - y2)) <= 8)
    {
      x = 0;
      y = pp_maze_get_maze_height (maze);
    }

  if (target_x)
    *target_x = x;
  if (target_y)
    *target_y = y;
}

static gboolean
pp_main_stage_captured_event_cb (ClutterActor *stage,
                                 ClutterEvent *event,
                                 PPPlayer     *player)
{
  gboolean unset;
  ClutterKeyEvent *key_event = (ClutterKeyEvent *)event;

  if (event->type != CLUTTER_KEY_PRESS &&
      event->type != CLUTTER_KEY_RELEASE)
    return FALSE;

  unset = (event->type == CLUTTER_KEY_PRESS) ? FALSE : TRUE;

  switch (key_event->keyval)
    {
    case CLUTTER_Up:
      pp_player_set_direction (player, PP_UP, unset);
      return TRUE;
    case CLUTTER_Right:
      pp_player_set_direction (player, PP_RIGHT, unset);
      return TRUE;
    case CLUTTER_Down:
      pp_player_set_direction (player, PP_DOWN, unset);
      return TRUE;
    case CLUTTER_Left:
      pp_player_set_direction (player, PP_LEFT, unset);
      return TRUE;
    }

  return FALSE;
}

static void
pp_menu_set_visible (PPData *data, gboolean visible)
{
  /* So naughty... */
  clutter_container_foreach (CLUTTER_CONTAINER (data->menu),
                             (ClutterCallback)mx_widget_set_disabled,
                             GINT_TO_POINTER (!visible));
  clutter_actor_animate (data->menu, CLUTTER_EASE_OUT_QUAD, 250,
                         "opacity", visible ? 0xff : 0x00,
                         NULL);
}

static void
pp_round_start_cb (ClutterAnimation *animation, PPData *data)
{
  ClutterActor *stage = clutter_actor_get_stage (data->maze);
  ClutterActor *ready = (ClutterActor *)
    clutter_animation_get_object (animation);

  clutter_actor_destroy (ready);

  /* Add keyboard controls */
  g_signal_connect (stage, "captured-event",
                    G_CALLBACK (pp_main_stage_captured_event_cb), data->player);

  /* Begin game */
  pp_maze_play (PP_MAZE (data->maze));
}

static void
pp_ready_cb (ClutterAnimation *animation, PPData *data)
{
  ClutterTimeline *timeline;
  ClutterActor *ready = (ClutterActor *)
    clutter_animation_get_object (animation);

  /* Fade out and start the game after 2 seconds */
  animation = clutter_actor_animate (ready, CLUTTER_EASE_OUT_QUAD, 150,
                                     "opacity", 0x00, NULL);
  g_signal_connect_after (animation, "completed",
                          G_CALLBACK (pp_round_start_cb), data);
  timeline = clutter_animation_get_timeline (animation);
  clutter_timeline_stop (timeline);
  clutter_timeline_set_delay (timeline, 2000);
  clutter_timeline_start (timeline);
}

static gboolean
pp_round_start (PPData *data)
{
  ClutterActor *ready;
  ClutterAnimation *animation;

  /* Prepare maze */
  pp_maze_reset (PP_MAZE (data->maze));

  /* Show 'ready' message */
  ready = mx_label_new_with_text ("READY");
  mx_stylable_set_style_class (MX_STYLABLE (ready), "Game");
  clutter_actor_set_opacity (ready, 0x00);
  clutter_container_add_actor (CLUTTER_CONTAINER (data->screen_box), ready);
  animation = clutter_actor_animate (ready, CLUTTER_EASE_IN_QUAD, 150,
                                     "opacity", 0xff, NULL);
  g_signal_connect_after (animation, "completed",
                          G_CALLBACK (pp_ready_cb), data);

  return FALSE;
}

static void
pp_new_game_cb (ClutterActor *button, PPData *data)
{
  gint i;

  /* Fade out menu */
  pp_menu_set_visible (data, FALSE);

  /* Reset score/lives */
  data->score = 0;
  clutter_container_foreach (CLUTTER_CONTAINER (data->lives_box),
                             (ClutterCallback)clutter_actor_destroy, NULL);
  for (i = 0; i < 3; i++)
    {
      ClutterActor *life = pp_player_new ();
      clutter_actor_set_size (life, 12, 12);
      clutter_actor_set_scale (life, 12.0, 12.0);
      g_object_set (G_OBJECT (life), "mouth", 0.2, NULL);
      clutter_container_add_actor (CLUTTER_CONTAINER (data->lives_box), life);
    }

  pp_round_start (data);
}

static void
pp_high_scores_cb (ClutterActor *button, PPData *data)
{

}

static void
pp_quit_cb (ClutterActor *button, PPData *data)
{
  clutter_main_quit ();
}

static ClutterActor *
pp_create_menu (PPData *data)
{
  ClutterActor *layout, *item;

  layout = mx_box_layout_new ();
  mx_box_layout_set_orientation (MX_BOX_LAYOUT (layout),
                                 MX_ORIENTATION_VERTICAL);

  item = mx_button_new_with_label ("New game");
  g_signal_connect (item, "clicked",
                    G_CALLBACK (pp_new_game_cb), data);
  clutter_container_add_actor (CLUTTER_CONTAINER (layout), item);

  item = mx_button_new_with_label ("High-scores");
  g_signal_connect (item, "clicked",
                    G_CALLBACK (pp_high_scores_cb), data);
  clutter_container_add_actor (CLUTTER_CONTAINER (layout), item);

  item = mx_button_new_with_label ("Quit");
  g_signal_connect (item, "clicked",
                    G_CALLBACK (pp_quit_cb), data);
  clutter_container_add_actor (CLUTTER_CONTAINER (layout), item);

  mx_stylable_set_style_class (MX_STYLABLE (layout), "Menu");

  return layout;
}


static void
pp_game_finished_cb (ClutterAnimation *animation, PPData *data)
{
  ClutterActor *gameover = (ClutterActor *)
    clutter_animation_get_object (animation);

  clutter_actor_destroy (gameover);

  /* Show the main menu */
  pp_menu_set_visible (data, TRUE);
}

static void
pp_gameover_cb (ClutterAnimation *animation, PPData *data)
{
  ClutterTimeline *timeline;
  ClutterActor *gameover = (ClutterActor *)
    clutter_animation_get_object (animation);

  /* Fade out and show the menu after 2 seconds */
  animation = clutter_actor_animate (gameover, CLUTTER_EASE_OUT_QUAD, 150,
                                     "opacity", 0x00, NULL);
  g_signal_connect_after (animation, "completed",
                          G_CALLBACK (pp_game_finished_cb), data);
  timeline = clutter_animation_get_timeline (animation);
  clutter_timeline_stop (timeline);
  clutter_timeline_set_delay (timeline, 2000);
  clutter_timeline_start (timeline);
}

static void
pp_main_player_die_cb (PPGuy *guy, PPData *data)
{
  GList *lives;
  ClutterActor *stage = clutter_actor_get_stage (data->maze);

  /* Remove keyboard controls and pause maze */
  g_signal_handlers_disconnect_by_func (stage,
                                        pp_main_stage_captured_event_cb,
                                        data->player);
  pp_maze_pause (PP_MAZE (data->maze));

  /* Check lives */
  lives = clutter_container_get_children (CLUTTER_CONTAINER (data->lives_box));
  if (!lives)
    {
      /* No lives, Game over */
      ClutterAnimation *animation;
      ClutterActor *gameover = mx_label_new_with_text ("GAME OVER");

      mx_stylable_set_style_class (MX_STYLABLE (gameover), "Game");
      clutter_actor_set_opacity (gameover, 0x00);
      clutter_container_add_actor (CLUTTER_CONTAINER (data->screen_box),
                                   gameover);

      animation = clutter_actor_animate (gameover, CLUTTER_EASE_IN_QUAD, 150,
                                         "opacity", 0xff, NULL);
      g_signal_connect_after (animation, "completed",
                              G_CALLBACK (pp_gameover_cb), data);

      return;
    }

  /* Remove a life and start a new round after a short pause for reflection */
  clutter_container_remove_actor (CLUTTER_CONTAINER (data->lives_box),
                                  CLUTTER_ACTOR (lives->data));
  g_list_free (lives);

  g_timeout_add_seconds (2, (GSourceFunc)pp_round_start, data);
}

static void
pp_main_player_collide_cb (PPGuy *guy, PPGuy *collidee, PPData *data)
{

}

static void
pp_main_player_passing_cb (PPGuy *guy, PPMazeTileCode *code, PPData *data)
{

}

int
main (int argc, char **argv)
{
  const ClutterColor black  = { 0x00, 0x00, 0x00, 0xff };
  const ClutterColor red    = { 0xff, 0x00, 0x00, 0xff };
  const ClutterColor cyan   = { 0x00, 0xff, 0xde, 0xff };
  const ClutterColor pink   = { 0xff, 0xb8, 0xde, 0xff };
  const ClutterColor orange = { 0xff, 0xb8, 0x47, 0xff };

  PPData data;
  PPGuy *guys[2];
  MxWindow *window;
  MxApplication *app;
  const gchar *aa_type;
  ClutterLayoutManager *layout;
  ClutterActor *stage, *box, *vbox, *hbox, *maze, *menu, *player, *enemy, *logo;

  app = mx_application_new (&argc, &argv, "pill-popper",
                            MX_APPLICATION_SINGLE_INSTANCE);
  mx_style_load_from_file (mx_style_get_default (), "pp.css", NULL);

  window = mx_application_create_window (app);
  stage = (ClutterActor *)mx_window_get_clutter_stage (window);
  clutter_stage_set_color (CLUTTER_STAGE (stage), &black);

  /* Create maze */
  data.maze = maze = pp_maze_new ();

  /* Create player */
  data.player = player = pp_player_new ();
  pp_maze_add_guy (PP_MAZE (maze),
                   PP_GUY (player),
                   PP_SPAWN_PLAYER);
  guys[0] = (PPGuy *)player;

  /* Connect to player signals for scoring, lives */
  g_signal_connect (player, "collide",
                    G_CALLBACK (pp_main_player_collide_cb), &data);
  g_signal_connect (player, "passing",
                    G_CALLBACK (pp_main_player_passing_cb), &data);
  g_signal_connect (player, "die",
                    G_CALLBACK (pp_main_player_die_cb), &data);

  /* Create enemies */
  enemy = pp_enemy_new (PP_PLAYER (player));
  g_object_set (G_OBJECT (enemy), "color", &red, NULL);
  pp_maze_add_guy (PP_MAZE (maze),
                   PP_GUY (enemy),
                   PP_SPAWN_ENEMY1);
  g_signal_connect (enemy, "targetting",
                    G_CALLBACK (pp_main_enemy1_targetting_cb),
                    player);
  guys[1] = (PPGuy *)enemy;

  enemy = pp_enemy_new ();
  g_object_set (G_OBJECT (enemy), "color", &cyan, NULL);
  pp_maze_add_guy (PP_MAZE (maze),
                   PP_GUY (enemy),
                   PP_SPAWN_ENEMY2);
  g_signal_connect (enemy, "targetting",
                    G_CALLBACK (pp_main_enemy2_targetting_cb),
                    player);

  enemy = pp_enemy_new (PP_PLAYER (player));
  g_object_set (G_OBJECT (enemy), "color", &pink, NULL);
  pp_maze_add_guy (PP_MAZE (maze),
                   PP_GUY (enemy),
                   PP_SPAWN_ENEMY3);
  g_signal_connect (enemy, "targetting",
                    G_CALLBACK (pp_main_enemy3_targetting_cb),
                    guys);

  enemy = pp_enemy_new (PP_PLAYER (player));
  g_object_set (G_OBJECT (enemy), "color", &orange, NULL);
  pp_maze_add_guy (PP_MAZE (maze),
                   PP_GUY (enemy),
                   PP_SPAWN_ENEMY4);
  g_signal_connect (enemy, "targetting",
                    G_CALLBACK (pp_main_enemy4_targetting_cb),
                    player);

  /* Create menu */
  data.menu = menu = pp_create_menu (&data);
  pp_maze_pause (PP_MAZE (maze));

  /* Create box and set as the window child */
  layout = clutter_bin_layout_new (CLUTTER_BIN_ALIGNMENT_FILL,
                                   CLUTTER_BIN_ALIGNMENT_FILL);
  data.screen_box = box = clutter_box_new (layout);
  clutter_container_add_actor (CLUTTER_CONTAINER (box), menu);
  clutter_bin_layout_set_alignment (CLUTTER_BIN_LAYOUT (layout),
                                    menu,
                                    CLUTTER_BIN_ALIGNMENT_CENTER,
                                    CLUTTER_BIN_ALIGNMENT_CENTER);

  mx_window_set_child (window, box);

  /* Add a vbox, top-half is the game, second half is the hbox with the
   * score and lives box.
   */
  vbox = mx_box_layout_new ();
  mx_box_layout_set_orientation (MX_BOX_LAYOUT (vbox), MX_ORIENTATION_VERTICAL);
  clutter_container_add_actor (CLUTTER_CONTAINER (box), vbox);
  clutter_actor_lower_bottom (vbox);

  /* Add maze to window and show */
  aa_type = g_getenv ("PP_AA_TYPE");
  if ((!aa_type || g_str_equal (aa_type, "super")) &&
      clutter_feature_available (CLUTTER_FEATURE_OFFSCREEN))
    {
      ClutterActor *bin = pp_super_aa_new ();

      pp_super_aa_set_resolution (PP_SUPER_AA (bin), 1.5, 1.5);
      clutter_container_add_actor (CLUTTER_CONTAINER (bin), maze);
      mx_box_layout_add_actor_with_properties (MX_BOX_LAYOUT (vbox), bin, 0,
                                               "expand", TRUE, NULL);

      /* Set the line width so super-aa works */
      glLineWidth (2);
    }
  else if (aa_type && g_str_equal (aa_type, "edge") &&
           clutter_feature_available (CLUTTER_FEATURE_SHADERS_GLSL))
    {
      ClutterActor *bin = pp_edge_aa_new ();

      clutter_container_add_actor (CLUTTER_CONTAINER (bin), maze);
      mx_box_layout_add_actor_with_properties (MX_BOX_LAYOUT (vbox), bin, 0,
                                               "expand", TRUE, NULL);
    }
  else
    mx_box_layout_add_actor_with_properties (MX_BOX_LAYOUT (vbox), maze, 0,
                                             "expand", TRUE, NULL);

  /* Add score label and lives box */
  hbox = mx_box_layout_new ();
  clutter_actor_set_name (hbox, "status-bar");
  clutter_container_add_actor (CLUTTER_CONTAINER (vbox), hbox);
  data.score_label = mx_label_new_with_text ("000000000");
  data.lives_box = mx_box_layout_new ();
  clutter_container_add (CLUTTER_CONTAINER (hbox),
                         data.score_label, data.lives_box, NULL);

  /* Make the default packing centre-aligned for game messages */
  g_object_set (G_OBJECT (layout),
                "x-align", CLUTTER_BIN_ALIGNMENT_CENTER,
                "y-align", CLUTTER_BIN_ALIGNMENT_CENTER,
                NULL);

  /* Add logo to toolbar */
  logo = clutter_texture_new_from_file ("logo.png", NULL);
  clutter_container_add_actor (CLUTTER_CONTAINER (
                                 mx_window_get_toolbar (window)), logo);
  mx_bin_set_fill (MX_BIN (mx_window_get_toolbar (window)), FALSE, FALSE);

  clutter_actor_show (stage);

  /* Following makes it look a bit nicer, but messing with the blend mode
   * messes up opacity... Should probably integrate this in PPMaze somehow.
   */
/*  glEnable (GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable (GL_POINT_SMOOTH);
  glEnable (GL_LINE_SMOOTH);
  glEnable (GL_POLYGON_SMOOTH);
  glHint (GL_POINT_SMOOTH_HINT, GL_NICEST);
  glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);
  glHint (GL_POLYGON_SMOOTH_HINT, GL_NICEST);*/

  /* Start */
  mx_application_run (app);

  return 0;
}
