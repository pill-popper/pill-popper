
#ifndef _PP_TYPES_H
#define _PP_TYPES_H

typedef struct _PPGuy PPGuy;
typedef struct _PPMaze PPMaze;

typedef guchar PPMazeTileCode;

enum
{
  PP_EMPTY          = 0,
  PP_WALL           = 1,
  PP_PILL           = 2,
  PP_SUPER_PILL     = 3,
  PP_SPAWN_PLAYER   = 4,
  PP_BARRIER_ENEMY  = 5,
  PP_SPAWN_ENEMY1   = 6,
  PP_SPAWN_ENEMY2   = 7,
  PP_SPAWN_ENEMY3   = 8,
  PP_SPAWN_ENEMY4   = 9,
  PP_BARRIER_PLAYER = 10,
  PP_SPAWN_BONUS    = 11,
};

typedef enum
{
  PP_STATIONARY,
  PP_UP,
  PP_DOWN,
  PP_LEFT,
  PP_RIGHT
} PPDirection;

#endif

