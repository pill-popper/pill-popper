
#include "pp-edge-aa.h"

G_DEFINE_TYPE (PPEdgeAA, pp_edge_aa, MX_TYPE_OFFSCREEN)

static gchar *edge_aa =
"uniform sampler2D tex;\n"
"uniform float x_step, y_step;\n"

"vec4 get_col(float dx, float dy)\n"
"  {\n"
"    vec4 color = texture2D (tex, gl_TexCoord[0].st + vec2(dx, dy));\n"
"    return color;\n"
"  }\n"

"float get_lum(vec4 color)\n"
"  {\n"
"    return (color.r + color.g + color.b) / 3.0;\n"
"  }\n"

"void main ()\n"
"  {\n"
"    vec4 color;\n"
"    vec4 sample[9];\n"

/* Take 3x3 sample around pixel */
"    sample[0] = get_col (-x_step, -y_step);\n"
"    sample[1] = get_col (0, -y_step);\n"
"    sample[2] = get_col (x_step, -y_step);\n"
"    sample[3] = get_col (-x_step, 0);\n"
"    sample[4] = get_col (0, 0);\n"
"    sample[5] = get_col (x_step, 0);\n"
"    sample[6] = get_col (-x_step, y_step);\n"
"    sample[7] = get_col (0, y_step);\n"
"    sample[8] = get_col (x_step, y_step);\n"

/* Work out x,y vector using Sobel filter */
"    float vy = -((get_lum(sample[0]) * 1) +\n"
"                 (get_lum(sample[1]) * 2) +\n"
"                 (get_lum(sample[2]) * 1) +\n"
"                 (get_lum(sample[6]) * -1) +\n"
"                 (get_lum(sample[7]) * -2) +\n"
"                 (get_lum(sample[8]) * -1));\n"
"    float vx = -((get_lum(sample[0]) * -1) +\n"
"                 (get_lum(sample[2]) * 1) +\n"
"                 (get_lum(sample[3]) * -2) +\n"
"                 (get_lum(sample[5]) * 2) +\n"
"                 (get_lum(sample[6]) * -1) +\n"
"                 (get_lum(sample[8]) * 1));\n"
/*"    float vz = 1.0;\n"*/

/* Get the magnitude (vz is 1, explaining the +1) */
"    float mag = sqrt ((vx*vx)+(vy*vy)+1);\n"

/* If this was a straight sobel filter, these would be the colours */
/*"    color.r = ((vx / mag) + 1.0) / 2.0;\n"
"    color.g = ((vy / mag) + 1.0) / 2.0;\n"
"    color.b = ((vz / mag) + 1.0) / 2.0;\n"
"    color.a = 1.0;\n"*/

/* Multiply the x and y vectors to get a value representing how
 * 'diagonal' we are.
 */
"    float x = (vx / mag);\n"
"    float y = (vy / mag);\n"
"    float mult = abs(x) * abs(y) * 0.5;\n"
"    float d = (mult * 8) + 1;\n"

/* Do a 3x3 box-blur, weighted with the above value */
"    color.r = ((sample[0].r * mult) +\n"
"               (sample[1].r * mult) +\n"
"               (sample[2].r * mult) +\n"
"               (sample[3].r * mult) +\n"
"               (sample[4].r) +\n"
"               (sample[5].r * mult) +\n"
"               (sample[6].r * mult) +\n"
"               (sample[7].r * mult) +\n"
"               (sample[8].r * mult)) / d;\n"
"    color.g = ((sample[0].g * mult) +\n"
"               (sample[1].g * mult) +\n"
"               (sample[2].g * mult) +\n"
"               (sample[3].g * mult) +\n"
"               (sample[4].g) +\n"
"               (sample[5].g * mult) +\n"
"               (sample[6].g * mult) +\n"
"               (sample[7].g * mult) +\n"
"               (sample[8].g * mult)) / d;\n"
"    color.b = ((sample[0].b * mult) +\n"
"               (sample[1].b * mult) +\n"
"               (sample[2].b * mult) +\n"
"               (sample[3].b * mult) +\n"
"               (sample[4].b) +\n"
"               (sample[5].b * mult) +\n"
"               (sample[6].b * mult) +\n"
"               (sample[7].b * mult) +\n"
"               (sample[8].b * mult)) / d;\n"
"    color.a = sample[4].a;\n"

"    gl_FragColor = color * gl_Color;\n"
"  }\n";

static int
next_p2 (gint a)
{
  int rval = 1;

  while (rval < a)
    rval <<= 1;

  return rval;
}

static void
pp_edge_aa_allocate (ClutterActor           *actor,
                     const ClutterActorBox  *box,
                     ClutterAllocationFlags  flags)
{
  ClutterShader *shader;

  CLUTTER_ACTOR_CLASS (pp_edge_aa_parent_class)->allocate (actor, box, flags);

  shader = clutter_actor_get_shader (actor);
  if (shader)
    {
      clutter_actor_set_shader_param_float (actor, "x_step",
                                            1.0f / next_p2 (box->x2 - box->x1));
      clutter_actor_set_shader_param_float (actor, "y_step",
                                            1.0f / next_p2 (box->y2 - box->y1));
    }
}

static void
pp_edge_aa_class_init (PPEdgeAAClass *klass)
{
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  actor_class->allocate = pp_edge_aa_allocate;
}

static void
pp_edge_aa_init (PPEdgeAA *self)
{
  ClutterShader *shader;
  GError *error = NULL;

  shader = clutter_shader_new ();

  clutter_shader_set_fragment_source (shader, edge_aa, -1);

  if (!clutter_shader_compile (shader, &error))
    {
      g_warning ("Failed to compile shader:\n%s", error->message);
      g_error_free (error);
      g_object_unref (shader);
      return;
    }

  clutter_actor_set_shader (CLUTTER_ACTOR (self), shader);
  clutter_actor_set_shader_param_int (CLUTTER_ACTOR (self), "tex", 0);

  g_object_unref (shader);
}

ClutterActor *
pp_edge_aa_new (void)
{
  return g_object_new (PP_TYPE_EDGE_AA, NULL);
}

