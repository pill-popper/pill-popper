/* pp-enemy.h */

#ifndef _PP_ENEMY_H
#define _PP_ENEMY_H

#include <glib-object.h>
#include "pp-guy.h"
#include "pp-player.h"

G_BEGIN_DECLS

#define PP_TYPE_ENEMY pp_enemy_get_type()

#define PP_ENEMY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  PP_TYPE_ENEMY, PPEnemy))

#define PP_ENEMY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  PP_TYPE_ENEMY, PPEnemyClass))

#define PP_IS_ENEMY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  PP_TYPE_ENEMY))

#define PP_IS_ENEMY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  PP_TYPE_ENEMY))

#define PP_ENEMY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  PP_TYPE_ENEMY, PPEnemyClass))

typedef struct _PPEnemy PPEnemy;
typedef struct _PPEnemyClass PPEnemyClass;
typedef struct _PPEnemyPrivate PPEnemyPrivate;

struct _PPEnemy
{
  PPGuy parent;

  PPEnemyPrivate *priv;
};

struct _PPEnemyClass
{
  PPGuyClass parent_class;

  /* signals */
  void (*targetting) (PPGuy *guy, gint *x, gint *y);
};

GType pp_enemy_get_type (void) G_GNUC_CONST;

ClutterActor *pp_enemy_new ();

gboolean pp_enemy_is_vulnerable (PPEnemy *enemy);

G_END_DECLS

#endif /* _PP_ENEMY_H */
