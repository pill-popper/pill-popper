/* pp-maze.c */

#include <string.h>
#include "pp-maze.h"
#include "pp-player.h"
#include "pp-utils.h"

G_DEFINE_TYPE (PPMaze, pp_maze, CLUTTER_TYPE_ACTOR)

#define MAZE_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), PP_TYPE_MAZE, PPMazePrivate))

typedef struct
{
  PPGuy           *guy;
  PPMazeTileCode   spawn_point;
  gint             x;
  gint             y;
  PPDirection      direction;
  ClutterTimeline *timeline;
} PPGuyData;

struct _PPMazePrivate
{
  guchar          *maze;
  guchar          *maze_trace;
  guint            width;
  guint            height;
  ClutterColor     color;
  gboolean         paused;
  ClutterTimeline *safe_timeline;

  GList           *guys;
};

enum
{
  PROP_0,

  PROP_MAZE_WIDTH,
  PROP_MAZE_HEIGHT,
  PROP_COLOR,
  PROP_SAFE
};

static const ClutterColor blue = { 0x21, 0x21, 0xde, 0xff };
static const ClutterColor pill_color = { 0xff, 0xb8, 0x97, 0xff };

/* Default maze - should probably make a png for this.. */
/* For fun, highlight '1,' in vim :) */
static const guchar default_maze[] = {
  1,1,1,1,1,1,1,1,1,1,1,1,1, 1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,
  1,2,2,2,2,2,2,2,2,2,2,2,2, 1, 1,2,2,2,2,2,2,2,2,2,2,2,2,1,
  1,2,1,1,1,1,2,1,1,1,1,1,2, 1, 1,2,1,1,1,1,1,2,1,1,1,1,2,1,
  1,3,1,0,0,1,2,1,0,0,0,1,2, 1, 1,2,1,0,0,0,1,2,1,0,0,1,3,1,
  1,2,1,1,1,1,2,1,1,1,1,1,2, 1, 1,2,1,1,1,1,1,2,1,1,1,1,2,1,
  1,2,2,2,2,2,2,2,2,2,2,2,2, 2, 2,2,2,2,2,2,2,2,2,2,2,2,2,1,
  1,2,1,1,1,1,2,1,1,2,1,1,1, 1, 1,1,1,1,2,1,1,2,1,1,1,1,2,1,
  1,2,1,1,1,1,2,1,1,2,1,1,1, 0, 0,1,1,1,2,1,1,2,1,1,1,1,2,1,
  1,2,2,2,2,2,2,1,1,2,2,2,2, 1, 1,2,2,2,2,1,1,2,2,2,2,2,2,1,
  1,1,1,1,1,1,2,1,0,1,1,1,0, 1, 1,0,1,1,1,0,1,2,1,1,1,1,1,1,
  0,0,0,0,0,1,2,1,0,1,1,1,0, 1, 1,0,1,1,1,0,1,2,1,0,0,0,0,0,
  0,0,0,0,0,1,2,1,1,0,0,0,0, 0, 0,0,0,0,0,1,1,2,1,0,0,0,0,0,
  0,0,0,0,0,1,2,1,1,0,1,1,1,10,10,1,1,1,0,1,1,2,1,0,0,0,0,0,
  1,1,1,1,1,1,2,1,1,0,1,5,5, 0, 0,5,5,1,0,1,1,2,1,1,1,1,1,1,
  0,0,0,0,0,0,2,0,0,0,1,0,6, 7, 8,9,0,1,0,0,0,2,0,0,0,0,0,0,
  1,1,1,1,1,1,2,1,1,0,1,5,5, 5, 5,5,5,1,0,1,1,2,1,1,1,1,1,1,
  0,0,0,0,0,1,2,1,1,0,1,1,1, 1, 1,1,1,1,0,1,1,2,1,0,0,0,0,0,
  0,0,0,0,0,1,2,1,1,0,0,0,0,11,11,0,0,0,0,1,1,2,1,0,0,0,0,0,
  0,0,0,0,0,1,2,1,1,0,1,1,1, 1, 1,1,1,1,0,1,1,2,1,0,0,0,0,0,
  1,1,1,1,1,1,2,1,1,0,1,1,1, 0, 0,1,1,1,0,1,1,2,1,1,1,1,1,1,
  1,2,2,2,2,2,2,2,2,2,2,2,2, 1, 1,2,2,2,2,2,2,2,2,2,2,2,2,1,
  1,2,1,1,1,1,2,1,1,1,1,1,2, 1, 1,2,1,1,1,1,1,2,1,1,1,1,2,1,
  1,2,1,1,0,1,2,1,1,1,1,1,2, 1, 1,2,1,1,1,1,1,2,1,0,1,1,2,1,
  1,3,2,2,1,1,2,2,2,2,2,2,2, 4, 4,2,2,2,2,2,2,2,1,1,2,2,3,1,
  1,1,1,2,1,1,2,1,1,2,1,1,1, 1, 1,1,1,1,2,1,1,2,1,1,2,1,1,1,
  1,1,1,2,1,1,2,1,1,2,1,1,1, 0, 0,1,1,1,2,1,1,2,1,1,2,1,1,1,
  1,2,2,2,2,2,2,1,1,2,2,2,2, 1, 1,2,2,2,2,1,1,2,2,2,2,2,2,1,
  1,2,1,1,1,1,1,0,0,1,1,1,2, 1, 1,2,1,1,1,0,0,1,1,1,1,1,2,1,
  1,2,1,1,1,1,1,1,1,1,1,1,2, 1, 1,2,1,1,1,1,1,1,1,1,1,1,2,1,
  1,2,2,2,2,2,2,2,2,2,2,2,2, 2, 2,2,2,2,2,2,2,2,2,2,2,2,2,1,
  1,1,1,1,1,1,1,1,1,1,1,1,1, 1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1
};

static void
pp_maze_guy_direction_notify (PPGuy      *guy,
                              GParamSpec *pspec,
                              PPMaze     *self);

static void
pp_maze_get_property (GObject    *object,
                      guint       property_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  PPMaze *self = PP_MAZE (object);

  switch (property_id)
    {
    case PROP_MAZE_WIDTH:
      g_value_set_uint (value, pp_maze_get_maze_width (self));
      break;

    case PROP_MAZE_HEIGHT:
      g_value_set_uint (value, pp_maze_get_maze_height (self));
      break;

    case PROP_COLOR:
      clutter_value_set_color (value, &self->priv->color);
      break;

    case PROP_SAFE:
      g_value_set_uint (value, pp_maze_get_safe (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
pp_maze_set_property (GObject      *object,
                      guint         property_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  PPMaze *self = PP_MAZE (object);

  switch (property_id)
    {
    case PROP_MAZE_WIDTH:
      pp_maze_set_maze_width (self, g_value_get_uint (value));
      break;

    case PROP_MAZE_HEIGHT:
      pp_maze_set_maze_height (self, g_value_get_uint (value));
      break;

    case PROP_COLOR:
      pp_maze_set_color (self, clutter_value_get_color (value));
      break;

    case PROP_SAFE:
      pp_maze_set_safe (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
pp_maze_dispose (GObject *object)
{
  PPMaze *self = PP_MAZE (object);
  PPMazePrivate *priv = self->priv;

  if (priv->safe_timeline)
    {
      g_object_unref (priv->safe_timeline);
      priv->safe_timeline = NULL;
    }

  while (priv->guys)
    {
      PPGuyData *data = priv->guys->data;
      pp_maze_remove_guy (self, data->guy);
      priv->guys = g_list_delete_link (priv->guys, priv->guys);
    }

  G_OBJECT_CLASS (pp_maze_parent_class)->dispose (object);
}

static void
pp_maze_finalize (GObject *object)
{
  PPMazePrivate *priv = PP_MAZE (object)->priv;

  g_free (priv->maze);
  g_free (priv->maze_trace);

  G_OBJECT_CLASS (pp_maze_parent_class)->finalize (object);
}

static void
pp_maze_get_preferred_width (ClutterActor *actor,
                             gfloat        for_height,
                             gfloat       *min_width_p,
                             gfloat       *nat_width_p)
{
  PPMazePrivate *priv = PP_MAZE (actor)->priv;

  if (min_width_p)
    *min_width_p = priv->width * 16;

  if (nat_width_p)
    {
      if (for_height > 0)
        *nat_width_p = for_height / priv->height * priv->width;
      else
        *nat_width_p = priv->width * 16;
    }
}

static void
pp_maze_get_preferred_height (ClutterActor *actor,
                              gfloat        for_width,
                              gfloat       *min_height_p,
                              gfloat       *nat_height_p)
{
  PPMazePrivate *priv = PP_MAZE (actor)->priv;

  if (min_height_p)
    *min_height_p = priv->height * 16;

  if (nat_height_p)
    {
      if (for_width > 0)
        *nat_height_p = for_width / priv->width * priv->height;
      else
        *nat_height_p = priv->height * 16;
    }
}

static void
pp_maze_paint (ClutterActor *actor)
{
  GList *g;
  gint x, y, idx;
  gfloat width, height, tile_width, tile_height, curve, xpadding, ypadding;

  PPMazePrivate *priv = PP_MAZE (actor)->priv;

  clutter_actor_get_size (actor, &width, &height);
  tile_width = (gint)(width / priv->width);
  tile_height = (gint)(height / priv->height);
  curve = (gint)MIN (tile_width / 4.f, tile_height / 4.f);
  xpadding = (gint)(tile_width / 3.f);
  ypadding = (gint)(tile_height / 3.f);

  /* This probably over-complicated bit of code traces the outline
   * of the maze and draws around it. This would probably be less
   * code if it was recursive...
   */
  memset (priv->maze_trace, 0, priv->width * priv->height * 4);
  for (y = 0, idx = 0; y < priv->height; y++, idx += priv->width)
    {
      for (x = 0; x < priv->width; x++)
        {
          gint sx, sy, direction;
          gboolean first;

          guchar value = priv->maze[idx + x];
          gfloat pill_radius = (gint)(curve / 2.f);

          switch (value)
            {
              case PP_WALL:
                if (priv->maze_trace[((idx + x) * 4) + 0] ||
                    priv->maze_trace[((idx + x) * 4) + 1] ||
                    priv->maze_trace[((idx + x) * 4) + 2] ||
                    priv->maze_trace[((idx + x) * 4) + 3])
                  continue;
                break;

              case PP_SUPER_PILL:
                pill_radius *= 2;
              case PP_PILL:
                cogl_set_source_color4ub (pill_color.red,
                                          pill_color.green,
                                          pill_color.blue,
                                          pill_color.alpha);
                cogl_path_new ();
                cogl_path_arc ((x * tile_width) + (gint)(tile_width/2),
                               (y * tile_height) + (gint)(tile_height/2),
                               pill_radius, pill_radius,
                               0, 360);
                cogl_path_fill ();
                continue;

              case PP_BARRIER_PLAYER:

              default:
                continue;
            }

          cogl_set_source_color4ub (priv->color.red,
                                    priv->color.green,
                                    priv->color.blue,
                                    priv->color.alpha);

          cogl_path_new ();
          cogl_path_move_to ((x * tile_width) + xpadding + curve,
                             (y * tile_height) + ypadding);

          sx = x;
          sy = y;
          first = TRUE;
          direction = 0;

          do {
            switch (direction)
              {
              case 0:
                /* Right */
                /* Mark the north edge */
                priv->maze_trace[(((sy * priv->width) + sx) * 4) + 0] = 1;
                priv->maze_trace[(((sy * priv->width) + sx) * 4) + 1] = 1;
                if (!first)
                  cogl_path_line_to (((sx + 1) * tile_width) - xpadding - curve,
                                     (sy * tile_height) + ypadding);
                else
                  first = FALSE;

                if ((sx + 1) < priv->width &&
                    (sy - 1) >= 0 &&
                    priv->maze[((sy - 1) * priv->width) + sx + 1] == PP_WALL)
                  {
                    /* Follow edge upwards */
                    sx ++;
                    cogl_path_arc ((sx * tile_width) + xpadding - curve,
                                   (sy * tile_height) + ypadding - curve,
                                   curve, curve,
                                   90, 0);
                    sy --;
                    direction = 3;
                  }
                else if ((sx + 1) < priv->width &&
                         priv->maze[(sy * priv->width) + sx + 1] == PP_WALL)
                  {
                    /* Continue following right */
                    sx ++;
                  }
                else
                  {
                    /* Follow edge downwards */
                    first = TRUE;
                    direction = 1;
                    cogl_path_arc (((sx + 1) * tile_width) - xpadding - curve,
                                   (sy * tile_height) + ypadding + curve,
                                   curve, curve,
                                   270, 360);
                  }
                break;

              case 1:
                /* Down */
                /* Mark the east edge */
                priv->maze_trace[(((sy * priv->width) + sx) * 4) + 1] = 1;
                priv->maze_trace[(((sy * priv->width) + sx) * 4) + 3] = 1;
                if (!first)
                  cogl_path_line_to (((sx + 1) * tile_width) - xpadding,
                                     ((sy + 1) * tile_height) - ypadding-curve);
                else
                  first = FALSE;

                if ((sx + 1) < priv->width &&
                    (sy + 1) < priv->height &&
                    priv->maze[((sy + 1) * priv->width) + sx + 1] == PP_WALL)
                  {
                    /* Follow edge rightwards */
                    sx ++;
                    sy ++;
                    cogl_path_arc ((sx * tile_width) - xpadding + curve,
                                   (sy * tile_height) + ypadding - curve,
                                   curve, curve,
                                   180, 90);
                    direction = 0;
                  }
                else if ((sy + 1) < priv->height &&
                         priv->maze[((sy + 1) * priv->width) + sx] == PP_WALL)
                  {
                    /* Continue following downwards */
                    sy ++;
                  }
                else
                  {
                    /* Follow edge leftwards */
                    first = TRUE;
                    direction = 2;
                    cogl_path_arc (((sx + 1) * tile_width) - xpadding - curve,
                                   ((sy + 1) * tile_height) - ypadding - curve,
                                   curve, curve,
                                   0, 90);
                  }
                break;

              case 2:
                /* Left */
                /* Mark the south edge */
                priv->maze_trace[(((sy * priv->width) + sx) * 4) + 2] = 1;
                priv->maze_trace[(((sy * priv->width) + sx) * 4) + 3] = 1;
                if (!first)
                  cogl_path_line_to ((sx * tile_width) + xpadding + curve,
                                     ((sy + 1) * tile_height) - ypadding);
                else
                  first = FALSE;

                if ((sx - 1) >= 0 &&
                    (sy + 1) < priv->height &&
                    priv->maze[((sy + 1) * priv->width) + sx - 1] == PP_WALL)
                  {
                    /* Follow edge downwards */
                    sy ++;
                    cogl_path_arc ((sx * tile_width) - xpadding + curve,
                                   (sy * tile_height) - ypadding + curve,
                                   curve, curve,
                                   270, 180);
                    sx --;
                    direction = 1;
                  }
                else if ((sx - 1) >= 0 &&
                         priv->maze[(sy * priv->width) + sx - 1] == PP_WALL)
                  {
                    /* Continue following leftwards */
                    sx --;
                  }
                else
                  {
                    /* Follow edge upwards */
                    first = TRUE;
                    direction = 3;
                    cogl_path_arc ((sx * tile_width) + xpadding + curve,
                                   ((sy + 1) * tile_height) - ypadding - curve,
                                   curve, curve,
                                   90, 180);
                  }
                break;

              case 3:
                /* Up */
                /* Mark the west edge */
                priv->maze_trace[(((sy * priv->width) + sx) * 4) + 2] = 1;
                priv->maze_trace[(((sy * priv->width) + sx) * 4) + 0] = 1;
                if (!first)
                  cogl_path_line_to ((sx * tile_width) + xpadding,
                                     (sy * tile_height) + ypadding + curve);
                else
                  first = FALSE;

                if ((sy - 1) >= 0 &&
                    (sx - 1) >= 0 &&
                    priv->maze[((sy - 1) * priv->width) + sx - 1] == PP_WALL)
                  {
                    /* Follow edge leftwards */
                    cogl_path_arc ((sx * tile_width) + xpadding - curve,
                                   (sy * tile_height) - ypadding + curve,
                                   curve, curve,
                                   360, 270);
                    sx --;
                    sy --;
                    direction = 2;
                  }
                else if ((sy - 1) >= 0 &&
                         priv->maze[((sy - 1) * priv->width) + sx] == PP_WALL)
                  {
                    /* Continue following upwards */
                    sy --;
                  }
                else
                  {
                    /* Follow edge rightwards */
                    first = TRUE;
                    direction = 0;
                    cogl_path_arc ((sx * tile_width) + xpadding + curve,
                                   (sy * tile_height) + ypadding + curve,
                                   curve, curve,
                                   180, 270);
                  }
                break;
              }
          } while (!priv->maze_trace[((idx + x) * 4) + 2]);

          if (direction != 0)
            cogl_path_arc ((sx * tile_width) + xpadding + curve,
                           (sy * tile_height) + ypadding + curve,
                           curve, curve,
                           180, 270);
          cogl_path_close ();
          cogl_path_stroke ();
        }
    }

  cogl_set_source_color4ub (pill_color.red,
                            pill_color.green,
                            pill_color.blue,
                            pill_color.alpha);
  cogl_path_new ();

  /* Draw the guys */
  for (g = priv->guys; g; g = g->next)
    {
      gfloat x, y;
      gdouble movement;
      PPDirection direction;

      PPGuyData *data = g->data;

      x = data->x;
      y = data->y;

      movement = pp_guy_get_movement (data->guy);
      direction = data->direction;

      /* This is done because we update the tile position half
       * way through the movement (to ease collision detection)
       */
      if (movement >= 0.5)
        movement = - (1.0 - movement);

      switch (direction)
        {
        case PP_UP:
          y -= movement;
          break;
        case PP_DOWN:
          y += movement;
          break;
        case PP_LEFT:
          x -= movement;
          break;
        case PP_RIGHT:
          x += movement;
          break;
        default:
          break;
        }

      cogl_push_matrix ();

      /* Translate/scale into position */
      cogl_translate (x * tile_width,
                      y * tile_height,
                      0);
      cogl_scale (tile_width,
                  tile_height,
                  1.0);

      clutter_actor_paint (CLUTTER_ACTOR (data->guy));

      cogl_pop_matrix ();
    }
}

static void
pp_maze_map (ClutterActor *actor)
{
  GList *g;
  PPMazePrivate *priv = PP_MAZE (actor)->priv;

  CLUTTER_ACTOR_CLASS (pp_maze_parent_class)->map (actor);

  for (g = priv->guys; g; g = g->next)
    {
      PPGuyData *data = g->data;
      clutter_actor_map ((ClutterActor *)data->guy);
    }
}

static void
pp_maze_unmap (ClutterActor *actor)
{
  GList *g;
  PPMazePrivate *priv = PP_MAZE (actor)->priv;

  CLUTTER_ACTOR_CLASS (pp_maze_parent_class)->unmap (actor);

  for (g = priv->guys; g; g = g->next)
    {
      PPGuyData *data = g->data;
      clutter_actor_unmap ((ClutterActor *)data->guy);
    }
}

static void
pp_maze_class_init (PPMazeClass *klass)
{
  GParamSpec *pspec;

  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  g_type_class_add_private (klass, sizeof (PPMazePrivate));

  object_class->get_property = pp_maze_get_property;
  object_class->set_property = pp_maze_set_property;
  object_class->dispose = pp_maze_dispose;
  object_class->finalize = pp_maze_finalize;

  actor_class->get_preferred_width = pp_maze_get_preferred_width;
  actor_class->get_preferred_height = pp_maze_get_preferred_height;
  actor_class->paint = pp_maze_paint;
  actor_class->map = pp_maze_map;
  actor_class->unmap = pp_maze_unmap;

  pspec = g_param_spec_uint ("maze-width",
                             "Maze width",
                             "Width (in tiles) of the maze",
                             1, G_MAXUINT, 28,
                             G_PARAM_READWRITE |
                             G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_MAZE_WIDTH, pspec);

  pspec = g_param_spec_uint ("maze-height",
                             "Maze height",
                             "Height (in tiles) of the maze",
                             1, G_MAXUINT, 31,
                             G_PARAM_READWRITE |
                             G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_MAZE_WIDTH, pspec);

  pspec = clutter_param_spec_color ("color",
                                    "Maze color",
                                    "Color of the maze walls",
                                    &blue,
                                    G_PARAM_READWRITE |
                                    G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_COLOR, pspec);

  pspec = g_param_spec_uint ("safe",
                             "Safe",
                             "The amount of safe time left for players",
                             0, G_MAXUINT, 0,
                             G_PARAM_READWRITE |
                             G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_SAFE, pspec);
}

static void
pp_maze_init (PPMaze *self)
{
  PPMazePrivate *priv = self->priv = MAZE_PRIVATE (self);

  priv->color = blue;

  pp_maze_set_size (self, 28, 31);
  pp_maze_set_maze (self, default_maze);
}

ClutterActor *
pp_maze_new (void)
{
  return g_object_new (PP_TYPE_MAZE, NULL);
}

void
pp_maze_set_size (PPMaze *maze, guint width, guint height)
{
  guchar *maze_data;

  PPMazePrivate *priv = maze->priv;

  if ((priv->width == width) && (priv->height == height))
    return;

  maze_data = g_new0 (guchar, width * height);

  if (priv->maze)
    {
      gint x, y, idx, priv_idx;
      for (y = 0, idx = 0, priv_idx = 0; y < priv->height; y++)
        {
          for (x = 0; x < priv->width; x++)
            if ((x < width) && (y < height))
              maze_data[idx + x] = priv->maze[priv_idx + x];
          priv_idx += priv->width;
          idx += width;
        }
      g_free (priv->maze);
    }

  priv->width = width;
  priv->height = height;
  priv->maze = maze_data;

  g_free (priv->maze_trace);
  priv->maze_trace = g_new (guchar, width * height * 4);

  clutter_actor_queue_redraw (CLUTTER_ACTOR (maze));
}

guint
pp_maze_get_maze_width (PPMaze *maze)
{
  return maze->priv->width;
}

void
pp_maze_set_maze_width (PPMaze *maze, guint width)
{
  pp_maze_set_size (maze, width, maze->priv->height);
}

guint
pp_maze_get_maze_height (PPMaze *maze)
{
  return maze->priv->height;
}

void
pp_maze_set_maze_height (PPMaze *maze, guint height)
{
  pp_maze_set_size (maze, maze->priv->width, height);
}

void
pp_maze_get_color (PPMaze *maze, ClutterColor *color)
{
  if (color) *color = maze->priv->color;
}

void
pp_maze_set_color (PPMaze *maze, const ClutterColor *color)
{
  if (color)
    {
      maze->priv->color = *color;
      clutter_actor_queue_redraw (CLUTTER_ACTOR (maze));
    }
}

guint
pp_maze_get_safe (PPMaze *maze)
{
  PPMazePrivate *priv = maze->priv;

  if (priv->safe_timeline)
    return clutter_timeline_get_duration (priv->safe_timeline) -
           clutter_timeline_get_elapsed_time (priv->safe_timeline);
  else
    return 0;
}

static void
pp_maze_safe_new_frame_cb (ClutterTimeline *timeline,
                           gint             frame_num,
                           PPMaze          *maze)
{
  clutter_actor_queue_redraw (CLUTTER_ACTOR (maze));
}

static void
pp_maze_safe_completed_cb (ClutterTimeline *timeline,
                           PPMaze          *maze)
{
  PPMazePrivate *priv = maze->priv;

  g_object_unref (priv->safe_timeline);
  priv->safe_timeline = NULL;

  g_object_notify (G_OBJECT (maze), "safe");
  clutter_actor_queue_redraw (CLUTTER_ACTOR (maze));
}

void
pp_maze_set_safe (PPMaze *maze, guint msecs)
{
  PPMazePrivate *priv = maze->priv;

  if (!priv->safe_timeline)
    {
      priv->safe_timeline = clutter_timeline_new (msecs);
      g_signal_connect (priv->safe_timeline, "new-frame",
                        G_CALLBACK (pp_maze_safe_new_frame_cb), maze);
      g_signal_connect (priv->safe_timeline, "completed",
                        G_CALLBACK (pp_maze_safe_completed_cb), maze);
      g_object_notify (G_OBJECT (maze), "safe");
    }

  clutter_timeline_set_duration (priv->safe_timeline, msecs);
  clutter_timeline_rewind (priv->safe_timeline);
  clutter_timeline_start (priv->safe_timeline);
}

void
pp_maze_set_maze (PPMaze *maze, const guchar *data)
{
  PPMazePrivate *priv = maze->priv;
  memcpy (priv->maze, data, priv->width * priv->height);
}

guchar *
pp_maze_peek_maze (PPMaze *maze)
{
  return maze->priv->maze;
}

gint
pp_maze_guy_compare (gconstpointer a,
                     gconstpointer b)
{
  const PPGuyData *data = a;
  const PPGuy *guy = b;

  if (data->guy == guy)
    return 0;
  else
    return -1;
}

static PPGuyData *
pp_maze_guy_get_data (PPMaze *maze, PPGuy *guy)
{
  GList *d;

  d = g_list_find_custom (maze->priv->guys, guy, pp_maze_guy_compare);
  if (!d)
    return NULL;

  return (PPGuyData *)d->data;
}

gboolean
pp_maze_find_tile (PPMaze         *maze,
                   PPMazeTileCode  code,
                   gint           *x_ptr,
                   gint           *y_ptr)
{
  gint x, y;
  PPMazePrivate *priv = maze->priv;

  for (y = 0; y < priv->height; y++)
    {
      for (x = 0; x < priv->width; x++)
        {
          if (priv->maze[(y * priv->width) + x] == code)
            {
              if (x_ptr)
                *x_ptr = x;
              if (y_ptr)
                *y_ptr = y;

              return TRUE;
            }
        }
    }

  return FALSE;
}

static void
pp_maze_reset_guy (PPMaze *maze, PPGuy *guy)
{
  gint x, y;
  PPGuyData *data;

  if (!(data = pp_maze_guy_get_data (maze, guy)))
    return;

  /* Scan through the maze and check where to spawn from */
  if (!pp_maze_find_tile (maze, data->spawn_point, &x, &y))
    {
      g_warning ("Spawn point %d not found", data->spawn_point);
      return;
    }

  data->x = x;
  data->y = y;

  if (data->timeline)
    {
      g_object_unref (data->timeline);
      data->timeline = NULL;
    }
  pp_guy_set_movement (guy, 0.0);

  pp_guy_spawn (guy);
}

void
pp_maze_reset (PPMaze *maze)
{
  GList *g;
  PPMazePrivate *priv = maze->priv;

  for (g = priv->guys; g; g = g->next)
    {
      PPGuyData *data = g->data;
      pp_maze_reset_guy (maze, data->guy);
    }

  if (priv->safe_timeline)
    {
      g_object_unref (priv->safe_timeline);
      priv->safe_timeline = NULL;
    }
}

void
pp_maze_pause (PPMaze *maze)
{
  GList *g;
  PPMazePrivate *priv = maze->priv;

  priv->paused = TRUE;
  for (g = priv->guys; g; g = g->next)
    {
      PPGuyData *data = g->data;
      if (data->timeline)
        clutter_timeline_pause (data->timeline);
    }

  if (priv->safe_timeline)
    clutter_timeline_pause (priv->safe_timeline);
}

void
pp_maze_play (PPMaze *maze)
{
  GList *g;
  PPMazePrivate *priv = maze->priv;

  priv->paused = FALSE;
  for (g = priv->guys; g; g = g->next)
    {
      PPGuyData *data = g->data;
      if (data->timeline)
        clutter_timeline_start (data->timeline);
      else
        pp_maze_guy_direction_notify (data->guy, NULL, maze);
    }

  if (priv->safe_timeline)
    clutter_timeline_start (priv->safe_timeline);
}

static PPMazeTileCode
pp_maze_validate_direction (PPMaze *self, gint x, gint y, PPDirection direction)
{
  PPMazePrivate *priv = self->priv;

  switch (direction)
    {
    case PP_UP:
      if (y > 0)
        return priv->maze[((y - 1) * priv->width) + x];
      else
        return PP_EMPTY;

    case PP_DOWN:
      if (y < priv->height - 1)
        return priv->maze[((y + 1) * priv->width) + x];
      else
        return PP_EMPTY;

    case PP_LEFT:
      if (x > 0)
        return priv->maze[(y * priv->width) + x - 1];
      else
        return PP_EMPTY;

    case PP_RIGHT:
      if (x < priv->width - 1)
        return priv->maze[(y * priv->width) + x + 1];
      else
        return PP_EMPTY;

    default:
      break;
    }

  return PP_EMPTY;
}

PPMazeTileCode
pp_maze_guy_get_tile (PPMaze *maze, PPGuy *guy)
{
  PPGuyData *data;
  PPMazePrivate *priv;

  if (!(data = pp_maze_guy_get_data (maze, guy)))
    return FALSE;

  priv = maze->priv;

  if (data->x >= 0 &&
      data->y >= 0 &&
      data->x < priv->width &&
      data->y < priv->height)
    return priv->maze[(data->y * priv->width) + data->x];
  else
    return PP_EMPTY;
}

gboolean
pp_maze_guy_validate_direction (PPMaze *maze, PPGuy *guy, PPDirection direction)
{
  PPGuyData *data;
  PPMazeTileCode code;

  if (!(data = pp_maze_guy_get_data (maze, guy)))
    return FALSE;

  code = pp_maze_validate_direction (maze, data->x, data->y, direction);
  return pp_guy_pass_through (guy, code);
}

void
pp_maze_guy_get_position (PPMaze *maze,
                          PPGuy *guy,
                          gint *x,
                          gint *y)
{
  PPGuyData *data;

  if (!(data = pp_maze_guy_get_data (maze, guy)))
    return;

  if (x)
    *x = data->x;
  if (y)
    *y = data->y;
}

PPMazeTileCode
pp_maze_guy_get_spawn_point (PPMaze *maze, PPGuy *guy)
{
  PPGuyData *data;

  if (!(data = pp_maze_guy_get_data (maze, guy)))
    return -1;

  return data->spawn_point;
}

static void
pp_maze_check_collisions (PPMaze *maze, PPGuy *guy)
{
  GList *g;
  PPGuyData *data;
  PPMazePrivate *priv = maze->priv;

  if (!(data = pp_maze_guy_get_data (maze, guy)))
    return;

  for (g = priv->guys; g; g = g->next)
    {
      PPGuyData *data2 = g->data;

      if (data2->guy == data->guy)
        continue;

      if ((data2->x == data->x) && (data2->y == data->y))
        {
          pp_guy_collide (data->guy, data2->guy);
          pp_guy_collide (data2->guy, data->guy);
        }
    }
}

static void
pp_maze_guy_timeline_new_frame_cb (ClutterTimeline *timeline,
                                   gint             frame_num,
                                   PPGuy           *guy)
{
  pp_guy_set_movement (guy, clutter_timeline_get_progress (timeline));
  pp_maze_check_collisions (pp_guy_get_maze (guy), guy);
}

static void
pp_maze_guy_timeline_marker_reached_cb (ClutterTimeline *timeline,
                                        const gchar     *marker_name,
                                        gint             frame_num,
                                        PPGuy           *guy)
{
  guchar *tile;
  PPGuyData *data;

  PPMaze *self = PP_MAZE (clutter_actor_get_parent (CLUTTER_ACTOR (guy)));
  PPMazePrivate *priv = self->priv;

  if (!g_str_equal (marker_name, "half-way"))
    return;

  if (!(data = pp_maze_guy_get_data (self, guy)))
    return;

  switch (data->direction)
    {
    case PP_UP:
      data->y --;
      break;
    case PP_RIGHT:
      data->x ++;
      break;
    case PP_DOWN:
      data->y ++;
      break;
    case PP_LEFT:
      data->x --;
      break;
    default:
      return;
    }

  /* Check for passing actions (like eating pills) */
  tile = &priv->maze[(data->y * priv->width) + data->x];
  pp_guy_passing (guy, tile);
}

static void
pp_maze_guy_timeline_completed_cb (ClutterTimeline *timeline,
                                   PPGuy           *guy)
{
  PPGuyData *data;

  PPMaze *self = PP_MAZE (clutter_actor_get_parent (CLUTTER_ACTOR (guy)));
  PPMazePrivate *priv = self->priv;

  if (!(data = pp_maze_guy_get_data (self, guy)))
    return;

  g_object_unref (timeline);
  data->timeline = NULL;

  pp_guy_set_movement (guy, 0);

  /* Wrap around or signal the guy to decide on the next move */
  if (data->x < 0)
    data->x = priv->width;
  else if (data->x >= priv->width)
    data->x = -1;
  else if (data->y < 0)
    data->y = priv->height;
  else if (data->y >= priv->height)
    data->y = -1;
  else
    pp_guy_decide (guy);

  /* Re-evaluate next movement */
  pp_maze_guy_direction_notify (guy, NULL, self);
}

static void
pp_maze_guy_direction_notify (PPGuy      *guy,
                              GParamSpec *pspec,
                              PPMaze     *self)
{
  guint speed;
  gboolean valid;
  PPGuyData *data;
  PPMazeTileCode code;

  PPDirection direction = pp_guy_get_direction (guy);
  gdouble movement = pp_guy_get_movement (guy);

  if (self->priv->paused)
    return;

  /* Nothing to be done */
  if (direction == PP_STATIONARY)
    return;

  if (!(data = pp_maze_guy_get_data (self, guy)))
    return;

  /* If we're in the middle of a movement, the only valid direction
   * is the opposite of the current one.
   */
  if (data->timeline)
    {
      if (direction == data->direction)
        return;

      if (!pp_direction_is_opposite (direction, data->direction))
        return;

      /* Swap direction */
      guint duration = clutter_timeline_get_duration (data->timeline);
      guint elapsed =
        clutter_timeline_get_elapsed_time (data->timeline);

      clutter_timeline_advance (data->timeline, duration - elapsed);
      data->direction = direction;
      pp_guy_set_movement (guy, 1.0 - movement);

      return;
    }

  code = pp_maze_validate_direction (self, data->x, data->y, direction);

  /* Freeze direction notification as we're calling PPGuy functions
   * that may affect it.
   */
  valid = pp_guy_pass_through (data->guy, code);

  if (!valid)
    {
      /* Invalid direction and we weren't moving, return */
      if (data->direction == PP_STATIONARY)
        return;

      /* The new direction isn't valid, try the old one */
      code = pp_maze_validate_direction (self, data->x, data->y,
                                         data->direction);
      valid = pp_guy_pass_through (data->guy, code);
      if (!valid)
        {
          /* We've stopped, set the direction as the user-set direction so
           * drawing looks correct.
           */
          data->direction = direction;
          return;
        }
      else
        direction = data->direction;
    }

  speed = pp_guy_get_speed (data->guy);

  data->direction = direction;
  data->timeline = clutter_timeline_new (speed);
  clutter_timeline_add_marker_at_time (data->timeline,
                                       "half-way",
                                       speed / 2);
  g_signal_connect (data->timeline, "new-frame",
                    G_CALLBACK (pp_maze_guy_timeline_new_frame_cb),
                    data->guy);
  g_signal_connect (data->timeline, "completed",
                    G_CALLBACK (pp_maze_guy_timeline_completed_cb),
                    data->guy);
  g_signal_connect (data->timeline, "marker-reached",
                    G_CALLBACK (pp_maze_guy_timeline_marker_reached_cb),
                    data->guy);
  clutter_timeline_start (data->timeline);
}

void
pp_maze_add_guy (PPMaze *maze, PPGuy *guy, PPMazeTileCode spawn_point)
{
  PPGuyData *data;
  PPMazePrivate *priv = maze->priv;

  data = g_new0 (PPGuyData, 1);
  data->guy = guy;
  data->spawn_point = spawn_point;
  g_signal_connect (guy, "notify::direction",
                    G_CALLBACK (pp_maze_guy_direction_notify), maze);

  priv->guys = g_list_prepend (priv->guys, data);

  clutter_actor_set_parent (CLUTTER_ACTOR (guy),
                            CLUTTER_ACTOR (maze));
  clutter_actor_queue_redraw (CLUTTER_ACTOR (maze));

  pp_maze_reset_guy (maze, guy);
}

void
pp_maze_remove_guy (PPMaze *maze, PPGuy *guy)
{
  GList *g;
  PPGuyData *data;

  PPMazePrivate *priv = maze->priv;

  g = g_list_find_custom (priv->guys, guy, pp_maze_guy_compare);
  if (!g)
    {
      g_warning (G_STRLOC "guy not found");
      return;
    }

  data = g->data;
  g_signal_handlers_disconnect_by_func (guy,
                                        pp_maze_guy_direction_notify,
                                        maze);
  if (data->timeline)
    {
      clutter_timeline_stop (data->timeline);
      g_object_unref (data->timeline);
    }
  g_free (g->data);

  priv->guys = g_list_delete_link (priv->guys, g);
}

GList *
pp_maze_get_guys (PPMaze *maze)
{
  GList *g, *guys;

  PPMazePrivate *priv = maze->priv;

  guys = NULL;
  for (g = priv->guys; g; g = g->next)
    {
      PPGuyData *data = (PPGuyData *)g->data;
      guys = g_list_prepend (guys, data->guy);
    }

  return guys;
}

PPDirection
pp_maze_guy_get_direction (PPMaze *maze, PPGuy *guy)
{
  GList *g;
  PPGuyData *data;

  PPMazePrivate *priv = maze->priv;

  g = g_list_find_custom (priv->guys, guy, pp_maze_guy_compare);
  if (!g)
    {
      g_warning (G_STRLOC "guy not found");
      return PP_STATIONARY;
    }

  data = g->data;
  return data->direction;
}
